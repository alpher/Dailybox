-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016-06-29 20:37:21
-- 服务器版本： 5.7.9
-- PHP Version: 7.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dailybox`
--

-- --------------------------------------------------------

--
-- 表的结构 `db_accounts`
--

DROP TABLE IF EXISTS `db_accounts`;
CREATE TABLE IF NOT EXISTS `db_accounts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tid` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `passwd` varchar(256) DEFAULT NULL,
  `memo` varchar(256) DEFAULT NULL,
  `salt` varchar(256) DEFAULT NULL,
  `addtime` int(10) UNSIGNED DEFAULT NULL,
  `edittime` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `db_sessions`
--

DROP TABLE IF EXISTS `db_sessions`;
CREATE TABLE `db_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `db_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- 表的结构 `db_accounttype`
--

DROP TABLE IF EXISTS `db_accounttype`;
CREATE TABLE IF NOT EXISTS `db_accounttype` (
  `tid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `typename` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- 表的结构 `db_attachments`
--

DROP TABLE IF EXISTS `db_attachments`;
CREATE TABLE IF NOT EXISTS `db_attachments` (
  `aid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `contents` mediumblob,
  `mimetype` varchar(50) NOT NULL,
  `extname` varchar(10) NOT NULL,
  `addtime` int(10) UNSIGNED DEFAULT NULL,
  `edittime` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `db_events`
--

DROP TABLE IF EXISTS `db_events`;
CREATE TABLE IF NOT EXISTS `db_events` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `ip` varchar(50) DEFAULT NULL,
  `addtime` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `db_events`
--

-- --------------------------------------------------------

--
-- 表的结构 `db_items`
--

DROP TABLE IF EXISTS `db_items`;
CREATE TABLE IF NOT EXISTS `db_items` (
  `itemid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `proid` int(10) UNSIGNED DEFAULT NULL,
  `item` varchar(50) NOT NULL,
  `sortrank` int(11) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- 表的结构 `db_notes`
--

DROP TABLE IF EXISTS `db_notes`;
CREATE TABLE IF NOT EXISTS `db_notes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tid` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `addtime` int(10) DEFAULT NULL,
  `edittime` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `db_notetype`
--

DROP TABLE IF EXISTS `db_notetype`;
CREATE TABLE IF NOT EXISTS `db_notetype` (
  `tid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `typename` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `db_projects`
--

DROP TABLE IF EXISTS `db_projects`;
CREATE TABLE IF NOT EXISTS `db_projects` (
  `proid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `project` varchar(50) NOT NULL,
  `addtime` int(10) NOT NULL,
  `endtime` int(10) DEFAULT NULL,
  `score` decimal(10,1) UNSIGNED DEFAULT NULL,
  `memo` varchar(250) DEFAULT NULL,
  `frequency` smallint(3) NOT NULL,
  PRIMARY KEY (`proid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `db_tracks`
--

DROP TABLE IF EXISTS `db_tracks`;
CREATE TABLE IF NOT EXISTS `db_tracks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `itemid` int(10) UNSIGNED NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL,
  `notetime` int(10) UNSIGNED NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- 表的结构 `db_users`
--

DROP TABLE IF EXISTS `db_users`;
CREATE TABLE IF NOT EXISTS `db_users` (
  `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `pwd` varchar(32) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `db_users`
--

INSERT INTO `db_users` (`uid`, `username`, `pwd`, `salt`, `email`) VALUES
(1, 'tester', 'a1c731afb50131ed5f9786b08f51ecd1', 'mrwTHAYS', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;