<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/projects_add.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/28
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>

<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('projects', '计划管理');?></li>
          <li><?php echo anchor('projects/edit', '添加计划');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <i class="glyphicon glyphicon-edit icon-large"></i>
            <?php echo $title;?>
          </div>
          <div class="panel-body">
            <?php echo form_open('projects/edit', array('proid'=>'form1'));?>
            <?php echo form_hidden('submit', '1');?>
            <?php echo form_hidden('proid', set_value('proid', $info['proid']));?>
              <fieldset>
                <div class="form-group">
                  <label class="control-label">计划名</label>
                  <input class="form-control" placeholder="计划名" name="project" value="<?php echo set_value('project', $info['project']);?>" type="text">
                </div>
                <div class="form-group">
                  <label class="control-label">频率(一次/天)</label>
                  <input class="form-control" placeholder="频率" name="frequency" value="<?php echo set_value('frequency', $info['frequency']);?>" type="number">
                </div>
                <div class="form-group">
                  <label class="control-label">得分</label>
                  <input class="form-control" placeholder="得分" name="score" value="<?php echo set_value('score', $info['score']);?>" type="number">
                </div>
                <div class="form-group">
                  <label class="control-label">开始日期</label>
                  <input class="form-control datetime" placeholder="开始日期" name="addtime" value="<?php echo set_value('addtime', $info['addtime']);?>" type="date">
                </div>
                <div class="form-group">
                  <label class="control-label">结束日期</label>
                  <input class="form-control datetime" placeholder="结束日期" name="endtime" value="<?php echo set_value('endtime', $info['endtime']);?>" type="date">
                </div>
                <div class="form-group">
                  <label class="control-label">备注</label>
                  <textarea name="memo" class="form-control"><?php echo set_value('memo', $info['memo']);?></textarea>
                </div>
              </fieldset>
              <div class="form-actions">              	
                <button class="btn btn-default" type="submit">提交</button>
                <?php echo anchor('projects', '取消', array('class'=>'btn'));?>
                <span class="text-warning btn">
                	<?php if(isset($message)) echo $message;?>
                </span>
              </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
jQuery(function($){
	$('#form1').validate({
		errorElement: 'span',
		rules: {
			project: {
				required: true
			},
			frequency: {
				required: true
			},
			addtime: {
				required: true
			},
			endtime: {
				required: true
			}
		},
		messages: {
			project: {
				required: '计划名不能为空'
			},
			frequency: {
				required: '频率不能为空',
			},
			addtime: {
				required: '开始日期'
			},
			endtime: {
				required: '结束日期'
			}
		}		
	});
});
</script>