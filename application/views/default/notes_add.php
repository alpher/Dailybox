<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/notes_add.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/28
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>

<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('notes', '日记管理');?></li>
          <li><?php echo anchor('notes/edit', '添加日记');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <i class="glyphicon glyphicon-edit icon-large"></i>
            <?php echo $title;?>
          </div>
          <div class="panel-body">
            <?php echo form_open('notes/edit', array('id'=>'form1'));?>
            <?php echo form_hidden('submit', '1');?>
            <?php echo form_hidden('id', set_value('id', $info['id']));?>
              <fieldset>
                <div class="form-group">
                  <label class="control-label">标题</label>
                  <input class="form-control" placeholder="标题" name="title" value="<?php echo set_value('title', $info['title']);?>" type="text">
                </div>
                <div class="form-group">
                  <label class="control-label">分类</label>
                  <div>
                  <?php 
                  	$tid = set_value('tid', $info['tid']);
                  	if($type) {
	                  	foreach($type as $k => $t) {
                        $dat = array(
                          'name' => 'tid',
                          'id' => 'tid' . $t->tid,
                          'value' => $t->tid
                        );
                        $checked = ($tid == $t->tid || (empty($tid) && $k == 0)) ? true : false;
                                  
                        echo '<label class="radio-inline">' . form_radio($dat, $t->tid, $checked) . $t->typename . '</label>';
                        //echo form_label($t->typename, 'tid' . $t->tid);
	                  	}
                  	}
                  ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">内容</label>
                  <textarea name="content" class="editor"><?php echo set_value('content', $info['content']);?></textarea>
                </div>
                <div class="form-group">
                  <label class="control-label">日期</label>
                  <input class="form-control datetime" name="addtime" value="<?php echo set_value('addtime', date('Y-m-d', $info['addtime']));?>" type="date">
                </div>
              </fieldset>
              <div class="form-actions">              	
                <button class="btn btn-default" type="submit">提交</button>
                <?php echo anchor('notes', '取消', array('class'=>'btn'));?>
                <span class="text-warning btn">
                	<?php if(isset($message)) echo $message;?>
                </span>
              </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
jQuery(function($)
	$('#form1').validate({
		errorElement: 'span',
		rules: {
			title: {
				required: true
			},
			content: {
				required: true
			},
			tid: {
				required: true
			}			
		},
		messages: {
			title: {
				required: '标题不能为空'
			},
			content: {
				required: '内容不能为空'
			},
			tid: {
				required: '请选择类别'
			}
		}		
	});
});
</script>