<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/editpwd.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/23
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>
<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
	  <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('users', '用户管理');?></li>
          <li class="active"><?php echo anchor('users/editpwd', '修改密码');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <i class="glyphicon glyphicon-edit"></i>
            <?php echo $title;?>            
          </div>
          <div class="panel-body">
            <?php echo form_open('users/editpwd', array('id'=>'form1'));?>
            <?php echo form_hidden('submit', '1');?>
              <fieldset>
                <div class="form-group">
                  <label class="control-label">用户名</label>
                  <p class="form-control-static"><?php echo $username;?></p>
                  <!--<input class="form-control" placeholder="用户名" disabled="disabled" name="username" value="<?php echo set_value('username', $username);?>" type="text">-->
                </div>
                <div class="form-group">
                  <label class="control-label">原密码</label>
                  <input class="form-control" placeholder="原密码" name="oldpassword" type="password">
                </div>
                <div class="form-group">
                  <label class="control-label">新密码</label>
                  <input class="form-control" placeholder="新密码" name="password" id="password" type="password">
                </div>
                <div class="form-group">
                  <label class="control-label">确认密码</label>
                  <input class="form-control" placeholder="确认密码" name="password1" id="password1" type="password">
                </div>
                <div class="form-group">
                  <label class="control-label">E-mail</label>
                  <input class="form-control" placeholder="E-mail" name="email" value="<?php echo set_value('email', $email);?>" type="text">
                </div>
              </fieldset>
              <div class="form-actions">              	
                <button class="btn btn-default" type="submit">提交</button>
                <?php echo anchor('users', '取消', array('class'=>'btn'));?>
                <span class="text-warning btn">
                	<?php echo validation_errors();?>
                	<?php if(isset($message)) echo $message;?>
                </span>
              </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
jQuery(function($){
	$('#form1').validate({
		errorElement: 'span',
		rules: {
			oldpassword: {
				required: true,
				minlength: 6
			},
			password: {
				minlength: 6
			},
			password1: {
				minlength: 6,
				equalTo: '#password'
			},
			email: {
				required: true,
				email: true
			}
		},
		messages: {
			oldpassword: {
				required: '请输入原密码',
				minlength: '长度不能小于6个字符'
			},
			password: {
				minlength: '长度不能小于6个字符'
			},
			password1: {
				minlength: '长度不能小于6个字符',
				equalTo: '两次输入的密码不一致'
			},
			email: {
				required: 'E-mail不能为空',
				email: 'E-mail地址无效'
			}
		}		
	});
});
</script>