<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/attachments.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>
<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
	  <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('attachments', '附件管理');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default grid">
          <div class="panel-heading">
            <?php echo anchor('attachments/edit', '<i class="glyphicon glyphicon-plus"></i> 添加附件', array('class' => 'btn btn-info'));?>
          </div>
          <table class="table">
            <thead>
              <tr>
                <th><i class="glyphicon glyphicon-sort-by-order-alt"></i></th>
                <th>名称</th>
                <th class="hidden-xs">添加日期</th>
                <th class="hidden-xs">编辑日期</th>
                <th class="actions">
                  	操作
                </th>
              </tr>
            </thead>
            <tbody> 
            <?php if($rows){?>
            <?php foreach($rows as $k => $row){?>             
              <tr>
                <td><?php echo ($k+1);?></td>
                <td><?php echo anchor('attachments/image/' . $row->aid, $row->title . $row->extname, array('target' => '_blank'));?></td>
                <td class="hidden-xs"><?php echo date('Y-m-d H:i:s', $row->addtime);?></td>
                <td class="hidden-xs"><?php echo date('Y-m-d H:i:s', $row->edittime);?></td>
                <td class="action">
                  <?php echo anchor('attachments/edit/'.$row->aid, 
                  		'<i class="glyphicon glyphicon-edit"></i>',
                  		array('class' => 'btn btn-info btn-xs')
					          );
                  ?>
                  <!--<span class="btn btn-danger btn-xs" data-row="<?php echo $row->aid;?>">
                  	<i class="glyphicon glyphicon-trash"></i>
                  </span>-->
                  <span class="btn btn-danger btn-xs" data-confirm="" data-url="<?php echo site_url('attachments/delete/'.$row->aid);?>"><i class="glyphicon glyphicon-trash"></i></span>
                </td>
              </tr>
            <?php }?>
            <?php }?>
            </tbody>
          </table>
          <div class="panel-footer">
            <ul class="pagination pagination-sm">
              <li></li>
              <?php echo $page_links;?>
            </ul>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view($theme.'/chip/bottom');?>
</body>