<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/manager.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/13
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>

<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li class="active"><?php echo anchor('manager', '功能概览');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <i class="icon-coffee icon-large"></i>
            	今日计划
          </div>          
          <?php if(isset($projects)){?>
          <?php foreach($projects as $project){?>
          <div class="panel-body filters">
            <div class="row">
              <div class="text-center">
              	【<?php echo $project['project'];?>】
              </div>
            </div>
          </div>
          <div class="panel-body">
            <fieldset>
              <?php foreach($project['items'] as $item){?>
                <div class="form-group">
                  <button class="btn<?php if($item->state == 1){echo ' btn-info disabled';}else{echo ' btn-primary';};?> btn-block" type="button" data-row="<?php echo $item->itemid;?>">
                    <?php echo $item->item;?>                    
                    <?php 
                  	if($item->state == 1){
                  		echo '<span class="text-success">'.$item->date.'</span>';
                  	} else {
						echo '<span class="text-warning">待完成</span>';
					} 
                  	?>
                  </button>
                </div>
              <?php }?>
            </fieldset>          
          </div>
		  <?php }}?>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
jQuery(function($){
	$('button.btn-primary').click(function(){
		var obj = $(this);
		var id = $(this).attr('data-row');
		
		$.get('<?php echo site_url('projects/record');?>/' + id, function(data){
			//判断data内容
			if(data == 'ok')
			{
				var dt = new Date();
				var text = dt.getFullYear() + '-' + dt.getMonth() + '-' + dt.getDate() + ' ' + dt.getHours() + ':' + dt.getMinutes();
				obj.removeClass('btn-primary');
				obj.addClass('btn-info disabled');
				obj.children('span').html(text).removeClass('text-warning').addClass('text-success');
			} else {
				$('<span>' + data + '</span>').appendTo(obj).delay(1000).fadeOut(400, function(){
					$(this).remove();
				});
			}
		});
	});	
});
</script>