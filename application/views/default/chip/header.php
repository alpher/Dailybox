<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/chip/header.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/06
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title;?> - <?php echo $app_name . ' ' . $app_version;?></title>
<?php echo link_tag('static/assets/css/application.css');?>
<?php 
if(isset($css_files)){
	foreach($css_files as $css){
		echo link_tag('static/assets/css/' . $css);
	}
}
?>
<script src="<?php echo $base_url;?>static/assets/js/application.js"></script>
<?php 
if(isset($js_files)){
foreach($js_files as $js){
?>
<script src="<?php echo $base_url . $js;?>"></script>
<?php 
}
}
?>
</head>
