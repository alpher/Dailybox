<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/chip/sidebar.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/23
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-sm-3 col-md-2 sidebar">
  <ul class="nav nav-sidebar">
    <li class="<?php if(stripos(uri_string(), 'projects') !== FALSE) echo 'active';?>"><?php echo anchor('projects', '<i class="icon-list-alt"></i> 计划管理');?></li>
  </ul>
  <ul class="nav nav-sidebar">
    <li class="<?php if(stripos(uri_string(), 'notes') !== FALSE) echo 'active';?>"><?php echo anchor('notes', '日记管理');?></li>
  </ul>
  <ul class="nav nav-sidebar">
    <li class="<?php if(stripos(uri_string(), 'accounts') !== FALSE) echo 'active';?>"><?php echo anchor('accounts', '<i class="icon-list-alt"></i> 账户管理');?></li>
  </ul>
  <ul class="nav nav-sidebar">
    <li class="<?php if(stripos(uri_string(), 'attachments') !== FALSE) echo 'active';?>"><?php echo anchor('attachments', '<i class="icon-list-alt"></i> 附件管理');?></li>
  </ul>
</div>