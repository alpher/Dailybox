<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/chip/navbar.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/23
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url('manager');?>"><?php echo $app_name;?></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><?php echo anchor('projects', '计划管理');?></li>
        <li><?php echo anchor('notes', '日记管理');?></li>
        <li><?php echo anchor('accounts', '账户管理');?></li>
        <li><?php echo anchor('attachments', '附件管理');?></li>
        <li><?php echo anchor('users/editpwd', '账户信息');?></li>
        <li><?php echo anchor('events', '日志管理');?></li>
        <li><?php echo anchor('users/logout', '退出');?></li>
      </ul>
      <form class="navbar-form navbar-right">
        <input type="text" class="form-control" name="key" placeholder="Search...">
      </form>
    </div>
  </div>
</nav>