<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/accounts.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>

<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('accounts', '账户管理');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default grid">
          <div class="panel-heading">
            <div class="btn-group">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                分类
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><?php echo anchor('accounts', '全部');?></li>
                <?php 
                  if($typelist):
                  foreach($typelist as $t):
                ?>
                <li><?php echo anchor('accounts/index/' . $t->tid, $t->typename);?></li>
                <?php
                  endforeach;
                  endif;
                ?>
              </ul>
            </div>
            <?php echo anchor('accounts/edit', '<i class="glyphicon glyphicon-plus"></i> 添加账户', array('class' => 'btn btn-info'));?>
            <?php echo anchor('accounts/category', '<i class="glyphicon glyphicon-align-justify"></i> 类别管理', array('class' => 'btn btn-info'));?>
          </div>
          <table class="table">
            <thead>
              <tr>
                <th><i class="glyphicon glyphicon-sort-by-order-alt"></i></th>
                <th class="hidden-xs">分类</th>
                <th>标题</th>
                <!--<th>账户信息</th>-->
                <th class="hidden-xs">编辑日期</th>
                <th class="actions">
                  	操作
                </th>
              </tr>
            </thead>
            <tbody> 
            <?php if($rows){?>
            <?php foreach($rows as $k => $row){?>
              <tr>
                <td><?php echo ($k+1);?></td>
                <td class="hidden-xs"><?php echo anchor('accounts/index/' . $row->tid, $row->typename);?></td>
                <td><a data-show="<?php echo site_url('accounts/show/'.$row->id);?>"><?php echo $row->title;?></a></td>
                <!--<td><?php echo anchor('accounts/edit/' . $row->id, $row->title);?></td>
                <td>
                	<span class="btn btn-xs" data-get="<?php echo $row->id;?>" title="">[+显示]</span>
                </td>-->
                <td class="hidden-xs"><?php echo date('Y-m-d H:i:s', $row->edittime);?></td>
                <td class="action">
                  <?php echo anchor('accounts/edit/'.$row->id, 
                  		'<i class="glyphicon glyphicon-edit"></i>',
                  		array('class' => 'btn btn-info btn-xs')
					);
                  ?>
                  <!--<span class="btn btn-danger btn-xs" data-row="<?php echo $row->id;?>">
                  	<i class="glyphicon glyphicon-trash"></i>
                  </span>-->
                  <span class="btn btn-danger btn-xs" data-confirm="" data-url="<?php echo site_url('accounts/delete/'.$row->id);?>"><i class="glyphicon glyphicon-trash"></i></span>
                </td>
              </tr>
            <?php }?>
            <?php }?>
            </tbody>
          </table>
          <div class="panel-footer">
            <ul class="pagination pagination-sm">
              <li></li>
              <?php echo $page_links;?>
            </ul>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view($theme.'/chip/bottom');?>
</body>