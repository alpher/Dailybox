<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/eventlist.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/25
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>    
<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('events', '日志管理');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default grid">
          <div class="panel-heading">
            <i class="glyphicon glyphicon-th-list"></i>
            <?php echo $title;?>
          </div>
          <table class="table">
            <thead>
              <tr>
                <th><i class="glyphicon glyphicon-sort-by-order-alt"></i></th>
                <th>标题</th>
                <th class="hidden-xs">内容</th>
                <th class="hidden-xs">IP</th>
                <th>时间</th>
                <th class="actions hidden-xs">
                  	操作
                </th>
              </tr>
            </thead>
            <tbody> 
            <?php if($evts){?>
            <?php foreach($evts as $k => $evt){?>             
              <tr>
                <td><?php echo ($k+1);?></td>
                <td><?php echo $evt->title;?></td>
                <td class="hidden-xs"><?php echo $evt->content;?></td>
                <td class="hidden-xs"><?php echo $evt->ip;?></td>
                <td><?php echo date('Y-m-d H:i:s', $evt->addtime);?></td>
                <td class="action hidden-xs">
                  <span class="btn btn-danger btn-xs" data-row="<?php echo $evt->id;?>">
                  	<i class="glyphicon glyphicon-trash"></i>
                  </span>
                </td>
              </tr>
            <?php }?>
            <?php }?>
            </tbody>
          </table>
          <div class="panel-footer">
            <ul class="pagination pagination-sm">
              <li></li>
              <?php echo $page_links;?>
            </ul>
            <div class='pull-right'>
              	<?php echo anchor('events/delete/', 
                  		'<i class="glyphicon glyphicon-trash"></i> 清空日志',
                  		array('class' => 'btn btn-danger icon-mini')
					);
                ?>
            </div>
           </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view($theme.'/chip/bottom');?>
</body>
<script>
$(function(){
	$('span[data-row]').click(function(){
		var obj = $(this);
		var id = $(this).attr('data-row');
		
		$.get('<?php echo site_url('events/ajax_delete');?>/' + id, function(data){
			// 动态加载返回信息，延时删除
			$('<p class="btn">' + data + '</p>').insertAfter(obj).delay(1000).fadeOut(400, function(){
				$(this).remove();

				//判断data内容, 实时清除对应行记录
				if(data == 'ok')
				{
					obj.parent('td').parent('tr').remove();
				}
			});
		});
	});	
});
</script>