<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/login.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/06
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<div class="container">
	<?php echo form_open('init/index', array('id'=>'frmlogin', 'class' => 'form-signin validate-form'));?>
    <?php echo form_hidden('submit', '1');?>
		<input type="hidden" id="state" value="0">
		<h1 class="form-signin-heading text-center"><?php echo $app_name;?></h1>
    <div class="form-group has-feedback">
      <label for="username" class="sr-only">用户名或Email</label>
      <input type="text" name="username" id="username" class="form-control" placeholder="用户名或Email" value="<?php echo set_value('username');?>" required autofocus>
    </div>
    <div class="form-group has-feedback">
      <label for="inputPassword" class="sr-only">密码</label>
      <input type="password" name="password" id="password" class="form-control encode-md5" placeholder="密码" required>
    </div>
		<div class="checkbox">
			<label>
				<input type="checkbox" value="remember-me"> 记住密码
			</label>
		</div>
		<button class="btn btn-lg btn-primary btn-block" type="submit"><i class="icon-key"></i> 登录</button>
		<span class="text-warning">
			<?php echo validation_errors();?>
			<?php if(isset($message)) echo $message;?>
		</span>
	</form>	
</div> <!-- /container -->
</body>
