<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/projects.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/28
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>

<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('projects', '计划管理');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default grid">
          <div class="panel-heading">
            <?php echo anchor('projects/edit', '<i class="glyphicon glyphicon-plus"></i> 添加计划', array('class' => 'btn btn-info'));?>
            <?php echo anchor('projects/report', '<i class="glyphicon glyphicon-calendar"></i> 计划报表', array('class' => 'btn btn-info'));?>
          </div>
          <table class="table">
            <thead>
              <tr>
                <th><i class="glyphicon glyphicon-sort-by-order-alt"></i></th>
                <th>计划名</th>
                <th class="hidden-xs">频率</th>
                <th class="hidden-xs">得分</th>
                <th class="hidden-xs">开始日期</th>
                <th class="hidden-xs">结束日期</th>
                <th class="actions">
                  	操作
                </th>
              </tr>
            </thead>
            <tbody> 
            <?php if($rows){?>
            <?php foreach($rows as $k => $row){?>
              <tr>
                <td><?php echo ($k+1);?></td>
                <td><?php echo anchor('projects/tracks/'.$row->proid, $row->project);?></td>
                <td class="hidden-xs"><?php echo $row->frequency;?>天</td>
                <td class="hidden-xs"><?php echo $row->score;?></td>
                <td class="hidden-xs"><?php echo date('Y-m-d', $row->addtime);?></td>
                <td class="hidden-xs"><?php echo date('Y-m-d', $row->endtime);?></td>
                <td class="action">
                  <?php echo anchor('projects/edit/'.$row->proid, 
                  		'<i class="glyphicon glyphicon-edit"></i>',
                  		array('class' => 'btn btn-info btn-xs')
					);
                  ?>
                  <?php echo anchor('projects/items/'.$row->proid, 
                  		'<i class="glyphicon glyphicon-align-justify"></i>',
                  		array('class' => 'btn btn-info btn-xs')
					);
                  ?>
                  <?php echo anchor('projects/tracks/'.$row->proid, 
                  		'<i class="glyphicon glyphglyphicon glyphicon-flag"></i>',
                  		array('class' => 'btn btn-info btn-xs')
					);
                  ?>
                  <span class="btn btn-danger btn-xs" data-confirm="" data-url="<?php echo site_url('projects/delete/'.$row->proid);?>"><i class="glyphicon glyphicon-trash"></i></span>
                </td>
              </tr>
            <?php }?>
            <?php }?>
            </tbody>
          </table>
          <div class="panel-footer">
            <ul class="pagination pagination-sm">
              <li></li>
              <?php echo $page_links;?>
            </ul>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view($theme.'/chip/bottom');?>
</body>