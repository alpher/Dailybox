<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/items.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/28
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>

<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('projects', '计划管理');?></li>
          <li><?php echo anchor('projects/items/'.$proid, $project);?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default grid">
          <div class="panel-heading">
            <?php echo anchor('projects', '<i class="glyphicon glyphicon-th-list"></i> 计划管理', array('class' => 'btn btn-info'));?>
            <?php echo anchor('projects/additems', '<i class="glyphicon glyphicon-plus"></i> 添加条目', array('class' => 'btn btn-info'));?>
          </div>
          <div class="panel-body">
            <?php echo form_open('projects/additems/' . $proid, array('id'=>'form1'));?>
            <?php echo form_hidden('submit', '1');?>
            <?php echo form_hidden('itemid', set_value('itemid', $info->itemid));?>
              <fieldset>
              	<div class="form-group">
                  <label class="control-label">【<?php echo $project;?>】</label>    
                </div>
                <div class="form-group">
                  <label class="control-label">条目名称</label>
                  <input class="form-control" placeholder="条目名称" name="item" value="<?php echo set_value('item', $info->item);?>" type="text">
                </div>
                <div class="form-group">
                  <label class="control-label">显示顺序</label>
                  <input class="form-control" placeholder="显示顺序" name="sortrank" value="<?php echo set_value('sortrank', $info->sortrank);?>" type="text">
                </div>
              </fieldset>
              <div class="form-actions">
                <button class="btn btn-default" type="submit">提交</button>
                <?php echo anchor('projects', '返回', array('class' => 'btn'));?>
                <span class="text-warning btn">
                	<?php if(isset($message)) echo $message;?>
                </span>
              </div>
            <?php echo form_close();?>
          </div>          
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
jQuery(function($){
	$('#form1').validate({
		errorElement: 'span',
		rules: {
			item: {
				required: true
			}		
		},
		messages: {
			typename: {
				required: '条目名称不能为空'
			}
		}		
	});
});
</script>