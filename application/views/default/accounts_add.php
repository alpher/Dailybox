<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/accounts_add.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/28
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>
<div class="container-fluid">
  <div class="row">
	  <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('accounts', '账户管理');?></li>
          <li><?php echo anchor('accounts/edit', '添加账户');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <i class="glyphicon glyphicon-tags"></i>
            <?php echo $title;?>            
          </div>
          <div class="panel-body">
            <?php echo form_open('accounts/edit', array('id'=>'form1'));?>
            <?php echo form_hidden('submit', '1');?>
            <?php echo form_hidden('id', set_value('id', $info['id']));?>
              <fieldset>
                <div class="form-group">
                  <label class="control-label">标题</label>
                  <input class="form-control" placeholder="标题" name="title" value="<?php echo set_value('title', $info['title']);?>" type="text">
                </div>
                <div class="form-group">
                  <label class="control-label">用户名</label>
                  <input class="form-control" placeholder="用户名" name="username" value="<?php echo set_value('username', $info['username']);?>" type="text">
                </div>
                <div class="form-group">
                  <label class="control-label">密码</label>
                  <input class="form-control" placeholder="密码" name="passwd" value="<?php echo set_value('passwd', $info['passwd']);?>" type="text">
                </div>
                <div class="form-group">
                  <label class="control-label">分类</label>
                  <div>
                  <?php 
                    $tid = set_value('tid', $info['tid']);
                    if($type) {
                      foreach($type as $k => $t) {
                        $dat = array(
                          'name' => 'tid',
                          'id' => 'tid' . $t->tid,
                          'value' => $t->tid
                        );
                        $checked = ($tid == $t->tid || (empty($tid) && $k == 0)) ? true : false;
                        
                        echo '<label class="radio-inline">'.form_radio($dat, $t->tid, $checked). $t->typename . '</label>';
                        //echo form_radio($dat, $t->tid, $checked);
                        //echo form_label($t->typename, 'tid' . $t->tid);
                      }
                    }
                  ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">备注</label>
                  <textarea class="form-control" name="memo" rows="5"><?php echo set_value('memo', $info['memo']);?></textarea>
                </div>
              </fieldset>
              <div class="form-actions">              	
                <button class="btn btn-default" type="submit">提交</button>
                <?php echo anchor('accounts', '取消', array('class'=>'btn'));?>
                <span class="text-warning btn">
                  <?php if(isset($message)) echo $message;?>
                </span>
              </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
jQuery(function($){
	$('#form1').validate({
		errorElement: 'span',
		rules: {
			title: {
				required: true
			},
			username: {
				required: true
			},
			passwd: {
				required: true
			},
			tid: {
				required: true
			}			
		},
		messages: {
			title: {
				required: '标题必须填写'
			},
			username: {
				required: '用户名不能为空'
			},
			passwd: {
				required: '密码不能为空'
			},
			tid: {
				required: '请选择类别'
			}
		}		
	});
});
</script>