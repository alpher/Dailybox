<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/userslist.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/25
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>

<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('users', '用户管理');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default grid">
          <div class="panel-heading">
            <i class="glyphicon glyphicon-th-list"></i>
            <?php echo $title;?>
          </div>
          <table class="table">
            <thead>
              <tr>
                <th><i class="glyphicon glyphicon-sort-by-order-alt"></i></th>
                <th>用户名</th>
                <th>邮箱</th>
                <th class="actions">
                  	操作
                </th>
              </tr>
            </thead>
            <tbody> 
            <?php if($users){?>
            <?php foreach($users as $user){?>             
              <tr>
                <td><?php echo $user->uid;?></td>
                <td><?php echo $user->username;?></td>
                <td><?php echo $user->email;?></td>
                <td class="action">
                  <?php echo anchor('users/editpwd/'.$user->uid, 
                  		'<i class="glyphicon glyphglyphicon glyphicon-edit"></i>',
                  		array('class' => 'btn btn-info btn-xs')
					);
                  ?>
                  <a class="btn btn-danger btn-xs disabled" href="#">
                    <i class="glyphicon glyphicon-trash"></i>
                  </a>
                </td>
              </tr>
            <?php }?>
            <?php }?>
            </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>

</script>