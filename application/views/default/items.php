<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/items.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/28
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>

<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('projects', '计划管理');?></li>
          <li><?php echo anchor('projects/items/' . $proid, '计划条目');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default grid">
          <div class="panel-heading">            
            <?php echo anchor('projects/additems/' . $proid, '<i class="glyphicon glyphicon-plus"></i> 添加条目', array('class' => 'btn btn-info'));?>
            <?php echo anchor('projects', '返回');?>
          </div> 
          <table class="table">
            <thead>
              <tr>
                <th><i class="glyphicon glyphicon-sort-by-order-alt"></i></th>
                <th>分类名称</th>
                <th class="hidden-xs">显示顺序</th>
                <th class="actions">
                  	操作
                </th>
              </tr>
            </thead>
            <tbody> 
            <?php if($rows){?>
            <?php foreach($rows as $k => $r){?>
              <tr>
                <td><?php echo ($k+1);?></td>
                <td><?php echo anchor('projects/additems/' . $proid . '/' . $r->itemid, $r->item);?></td>
                <td class="hidden-xs"><?php echo $r->sortrank;?></td>
                <td class="action">
                  <?php echo anchor('projects/additems/' . $proid . '/' . $r->itemid, 
                  		'<i class="glyphicon glyphicon-edit"></i>',
                  		array('class' => 'btn btn-info btn-xs')
					);
                  ?>
                  <!--<span class="btn btn-danger btn-xs" data-row="<?php echo $r->itemid;?>">
                  	<i class="glyphicon glyphicon-trash"></i>
                  </span>-->
                  <span class="btn btn-danger btn-xs" data-confirm="" data-url="<?php echo site_url('projects/deleteitems/'.$r->itemid);?>"><i class="glyphicon glyphicon-trash"></i></span>
                </td>
              </tr>
            <?php }?>
            <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</body>