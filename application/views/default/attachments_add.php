<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/attachments_add.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/28
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>
<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
	  <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('attachments', '附件管理');?></li>
          <li><?php echo anchor('attachments/edit', '添加附件');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <i class="glyphicon glyphicon-edit"></i>
            <?php echo $title;?>
          </div>
          <div class="panel-body">
            <?php echo form_open_multipart('attachments/edit', array('id'=>'form1'));?>
            <?php echo form_hidden('submit', '1');?>
            <?php echo form_hidden('aid', set_value('aid', $attach['aid']));?>
              <fieldset>
                <div class="form-group">
                  <label class="control-label">标题</label>
                  <input class="form-control" placeholder="标题" name="title" value="<?php echo set_value('title', $attach['title']);?>" type="text">
                </div>
                <div class="form-group">
                  <label class="control-label">文件</label>
                  <?php echo form_upload(array('name' => 'fileurl'));?>
                </div>
              </fieldset>
              <div class="form-actions">              	
                <button class="btn btn-default" type="submit">提交</button>
                <?php echo anchor('attachments', '取消', array('class'=>'btn'));?>
                <span class="text-warning btn">
                	<?php if(isset($message)) echo $message;?>
                </span>
              </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
jQuery(function($){
	$('#form1').validate({
		errorElement: 'span',
		rules: {
			title: {
				minlength: 2
			}
		},
		messages: {
			title: {
				minlength: '长度不能小于2个字符'
			}
		}		
	});
});
</script>