<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/notetype.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/28
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- Navbar -->
<?php $this->load->view($theme.'/chip/navbar');?>

<div class="container-fluid">
  <div class="row">
    <!-- Sidebar -->
    <?php $this->load->view($theme.'/chip/sidebar');?>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <!-- Tools -->
      <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
          <li class="title">主页</li>
          <li><?php echo anchor('notes/category', '日记类别');?></li>
        </ul>
      </section>
      <!-- Content -->
      <div id="content">
        <div class="panel panel-default grid">
          <div class="panel-heading">
            <?php echo anchor('notes/category', '<i class="glyphicon glyphicon-plus"></i> 添加类别', array('class' => 'btn btn-info'));?>
          </div>        
          <div class="panel-body">
            <?php echo form_open('notes/category', array('id'=>'form1'));?>
            <?php echo form_hidden('submit', '1');?>
            <?php echo form_hidden('tid', set_value('tid', $info->tid));?>
              <fieldset>
                <div class="form-group">
                  <label class="control-label">类别名称</label>
                  <input class="form-control" placeholder="类别名称" name="typename" value="<?php echo set_value('typename', $info->typename);?>" type="text">
                </div>
              </fieldset>
              <div class="form-actions">              	
                <button class="btn btn-default" type="submit">提交</button>
                <?php echo anchor('notes', '返回', array('class' => 'btn'));?>
                <span class="text-warning btn">
                	<?php if(isset($message)) echo $message;?>
                </span>
              </div>
            <?php echo form_close();?>
          </div>
          <table class="table">
            <thead>
              <tr>
                <th><i class="glyphicon glyphicon-sort-by-order-alt"></i></th>
                <th>分类名称</th>
                <th class="actions">
                  	操作
                </th>
              </tr>
            </thead>
            <tbody> 
            <?php if($type){?>
            <?php foreach($type as $k => $t){?>             
              <tr>
                <td><?php echo ($k+1);?></td>
                <td><?php echo anchor('notes/category/' . $t->tid, $t->typename);?></td>
                <td class="action">
                  <?php echo anchor('notes/category/'.$t->tid, 
                  		'<i class="glyphicon glyphicon-edit"></i>',
                  		array('class' => 'btn btn-info btn-xs')
					);
                  ?>
                  <!--<span class="btn btn-danger btn-xs" data-row="<?php echo $t->tid;?>">
                  	<i class="glyphicon glyphicon-trash"></i>
                  </span>-->
                  <span class="btn btn-danger btn-xs" data-confirm="" data-url="<?php echo site_url('notes/deletecategory/'.$t->tid);?>"><i class="glyphicon glyphicon-trash"></i></span>
                </td>
              </tr>
            <?php }?>
            <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script>
jQuery(function($){
	$('#form1').validate({
		errorElement: 'span',
		rules: {
			typename: {
				required: true
			}		
		},
		messages: {
			typename: {
				required: '类别名称不能为空'
			}
		}		
	});
});
</script>