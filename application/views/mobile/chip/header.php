<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/mobile/chip/header.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/03/03
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title;?> - <?php echo $app_name . ' ' . $app_version;?></title>
<?php echo link_tag('static/assets/bootstrap-3.3.5/css/bootstrap.min.css');?>
<?php echo link_tag('static/assets/bootstrap-3.3.5/css/bootstrap-theme.min.css');?>
<?php 
if(isset($css_files)){
	foreach($css_files as $css){
		echo link_tag($css);
	}
}
?>
<script src="<?php echo $base_url;?>static/assets/javascripts/jquery-2.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url;?>static/assets/bootstrap-3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
<?php 
if(isset($js_files)){
foreach($js_files as $js){
?>
<script src="<?php echo $base_url . $js;?>" type="text/javascript"></script>
<?php 
}
}
?>
</head>