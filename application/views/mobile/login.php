﻿<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/views/mobile/login.php
 * Description CodeIgniter View
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/03/03
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="container">
<?php echo form_open('init/index', array('id'=>'frmlogin', 'class'=>'form-signin'));?>
 <?php echo form_hidden('submit', '1');?>
	<p class="text-center"><img src="<?php echo base_url('assets/images/login.png');?>" alt="<?php echo $app_name . ' ' . $app_version;?>" class="img-circle"></p>
	<h2 class="form-signin-heading">登录</h2>
	<div class="input-group">
      <div class="input-group-addon"><i class="glyphicon glyphicon-user"></i></div>
      <input type="email" id="inputEmail" class="form-control" placeholder="用户名或Email" required autofocus>
    </div>
	<p></p>
	<div class="input-group">
      <div class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></div>
      <input type="password" id="inputPassword" class="form-control" placeholder="密码" required>
    </div>
	<button class="btn btn-primary btn-block" type="submit">登录</button>
  </form>

</div> <!-- /container -->
</body>
</html>