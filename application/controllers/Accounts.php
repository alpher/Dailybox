<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/controllers/Accounts.php
 * Description CodeIgniter Contoller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
	
		$this->load->model('accounts_model');
		$this->load->model('accounttype_model');
		
		$this->load->library('encryption');		
	}
	
	public function index($tid = 0/*, $key = '关键字'*/)
	{
		$data = array();
		$data['title'] = '账户管理';
		
		$this->load->library('Pagination');		
		
		// 判断是否有提交搜索
		$options = array();
		/*
		if($this->input->post('submit') == 1)
		{
			$key = $this->input->post('key');
		}
		*/		
		
		// 构造搜索字段
		$key = $this->input->get('key');
		$key = urldecode($key);
		if(!empty($key) && $key != '关键字')
		{
			$options = array(
					'title' => $key,
					'memo' => $key
			);
		}		
		$data['key'] = $key;
		
		if(intval($tid) > 0)
		{
			$options['tid'] = intval($tid);
		}
		
		// 获取数据
		$query_temp = $this->accounts_model->get($options);
		
		// 配置分页信息
		//$config['base_url'] = site_url('accounts/index/' . $tid . '/' . $key);
		$config['base_url'] = site_url('accounts/index/' . $tid);
		$config['total_rows'] = $query_temp->num_rows();
		$config['per_page'] = 20;
		//$config['uri_segment'] = 5;
		$config['uri_segment'] = 4;
		$config['first_link'] = '首页';
		$config['last_link'] = '尾页';

		// 格式		
		$config['first_tag_open'] =
		$config['last_tag_open'] =
		$config['next_tag_open'] =
		$config['prev_tag_open'] =
		$config['num_tag_open'] = '<li>';
		
		$config['first_tag_close'] =
		$config['last_tag_close'] =
		$config['next_tag_close'] =
		$config['prev_tag_close'] =
		$config['num_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$this->pagination->initialize($config);
		// 传参数给VIEW
		$data['page_links'] = $this->pagination->create_links();
		/*
		$config['perpage'] = 1;   //每页条数
		$config['part'] = 2;//当前页前后链接数量
		$config['url'] = 'accounts/index/' . $tid . '/' . urlencode($key); //url
		$config['seg'] = 5; //参数取 index.php之后的段数，默认为3，即index.php/control/function/18 这种形式
		$config['nowindex'] = $this->uri->segment($config['seg']) ? $this->uri->segment($config['seg']) : 1;//当前页
		$this->load->library('mypage_class');
		$countnum = $query_temp->num_rows(); //得到记录总数
		$config['total'] = $countnum;
		$this->mypage_class->initialize($config);

		$config['per_page'] = $config['perpage'];
		$config['total_rows'] = $config['total'];

		$data['page_links'] = $this->mypage_class->show($config['nowindex']);
		*/
		
		// 分页
		//$offset = $this->uri->segment(5);
		$offset = $this->uri->segment(4);
		$offset = ($offset < 0) ? 0 : $offset;
		$temp = floor($config['total_rows'] / $config['per_page']) * $config['per_page'];
		$offset = ($offset > $temp) ?  $temp : $offset;
		
		// 再次查询，活动要显示的数据
		$options['limit'] = $config['per_page'];
		$options['offset'] = $offset;
		$query = $this->accounts_model->get($options);
		
		// 获取类别
		$type = $this->accounttype_model->get();
		$temp = $query->result();
		
		$rows = array();
		// 加入类别
		foreach($temp as $row)
		{
			foreach ($type as $t)
			{				
				if($row->tid == $t->tid)
				{
					$row->typename = $t->typename;					
					break;
				}
			}
			
			/*
			$key = hex2bin($row->salt);
			
			$this->encryption->initialize(array('key' => $key));
							
			$username = $this->encryption->decrypt($row->username);
			$passwd = $this->encryption->decrypt($row->passwd);

			$row->username = $username;
			$row->passwd = $passwd;
			*/
			
			$rows[] = $row;
		}
		
		$data['rows'] = $rows;
		$data['typelist'] = $this->accounttype_model->get();

		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/accounts', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function edit($id = 0)
	{
		$data = array();
		
		if($this->input->post('submit') == 1)
		{
			$this->load->library('form_validation');
			
			//构造验证规则
			$rules = array(
					array(
							'field' => 'title',
							'label' => '标题',
							'rules' => 'required',
							'errors' => array(
									'required' => '%s不能为空'
							)
					),
					array(
							'field' => 'username',
							'label' => '用户名',
							'rules' => 'required',
							'errors' => array(
									'required' => '%s不能为空'
							)
					),
					array(
							'field' => 'passwd',
							'label' => '密码',
							'rules' => 'required',
							'errors' => array(
									'required' => '%s不能为空'
							)
					),
					array(
							'field' => 'tid',
							'label' => '类别',
							'rules' => 'required',
							'errors' => array(
									'required' => '请选择%s'
							)
					)
			);
			
			// 设置验证规则
			$this->form_validation->set_rules($rules);
			
			if($this->form_validation->run() == false)
			{
				$data['message'] = validation_errors();
			}
			else 
			{
				$id = intval($this->input->post('id'));
				
				$data['info'] = array();
				// 判断是否是编辑状态
				if($id > 0)
				{
					$query = $this->accounts_model->get(array('id' => $id, 'limit' => 1));
				
					if($query->num_rows() > 0)
					{
						$row = $query->row_array();
						$data['info'] = $row;
						
						$key = hex2bin($row['salt']);
						
						$this->encryption->initialize(
								array(
										'key' => $key
								)
						);
						
						$username = $this->encryption->encrypt($this->input->post('username'));
						$passwd = $this->encryption->encrypt($this->input->post('passwd'));
						
						$data['info']['title'] = $this->input->post('title');
						$data['info']['username'] = $username;
						$data['info']['passwd'] = $passwd;
						$data['info']['memo'] = $this->input->post('memo');
						$data['info']['tid'] = $this->input->post('tid');
						$data['info']['edittime'] = time();
						
						$this->accounts_model->update($data['info']);
						
						redirect('accounts');
					}
					else 
					{
						$data['message'] = '记录不存在或已被删除';
					}
				}
				else
				{
					$key = $this->encryption->create_key(16);
					
					$this->encryption->initialize(
							array(
									'key' => $key
							)
					);
					
					$username = $this->encryption->encrypt($this->input->post('username'));
					$passwd = $this->encryption->encrypt($this->input->post('passwd'));
					
					$data['info']['title'] = $this->input->post('title');
					$data['info']['username'] = $username;
					$data['info']['passwd'] = $passwd;
					$data['info']['memo'] = $this->input->post('memo');
					$data['info']['salt'] = bin2hex($key);
					$data['info']['tid'] = $this->input->post('tid');
					$data['info']['addtime'] = time();
					$data['info']['edittime'] = time();
					
					$this->accounts_model->add($data['info']);
					
					redirect('accounts');
				}				
			}			
		}
		else
		{
			if($id > 0)
			{
				$query = $this->accounts_model->get(array('id' => $id, 'limit' => 1));				
				
				if($query->num_rows() > 0)
				{
					$row = $query->row_array();
						
					$data['title'] = '编辑账户';
					//$data['info'] = $row;
					
					$key = hex2bin($row['salt']);
					
					// 解密
					$this->encryption->initialize(
							array(
									'key' => $key
							)
					);
					
					$row['username'] = $this->encryption->decrypt($row['username']);
					$row['passwd'] = $this->encryption->decrypt($row['passwd']);
					
					$data['info'] = $row;
				}
				else
				{
					$data['message'] = '记录不存在或已被删除';
				}
			}
			else
			{
				$data['title'] = '添加账户';
		
				$data['info'] = array(
						'id' => 0,
						'title' => '',
						'username' => '',
						'passwd' => '',
						'tid' => 0,
						'memo' => ''
				);
			}
		}
		
		// 类别
		$data['type'] = $this->accounttype_model->get();
		// 加载视图
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/accounts_add', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function show($id)
	{
		$id = intval($id);
		$query = $this->accounts_model->get(array('id' => $id, 'limit' => 1));
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			
			$key = hex2bin($row->salt);
				
			// 解密
			$this->encryption->initialize(
					array(
							'key' => $key
					)
			);
				
			$username = $this->encryption->decrypt($row->username);
			$passwd = $this->encryption->decrypt($row->passwd);
			
			echo sprintf('<ul><li>账号：%s</li><li>密码：%s</li><li>备注：<pre>%s</pre></li></ul>', $username, $passwd, $row->memo);
		}
		else
		{
			echo 'NULL';
		}
	}
		
	public function delete($id)
	{
		$id = intval($id);
		if($id > 0)
		{
			$num = $this->accounts_model->delete(array('id' => $id));
			
			if($num > 0)
			{
				echo 'ok';
			}
			else 
			{
				echo '记录不存在';
			}
		}
		else
		{
			echo '缺少参数';
		}
	}
	
	public function category($tid = 0)
	{
		$data['title'] = '账户类别';
		
		$tid = intval($tid);
		
		// 是否有提交表单
		if($this->input->post('submit') == 1)
		{
			$catid = intval($this->input->post('tid'));
			
			$typename = $this->input->post('typename');
			
			if(empty($typename))
			{
				$data['message'] = '类别名称不能为空';
			}
			else
			{
				if($catid > 0)
				{					
					$nums = $this->accounttype_model->update($catid, $typename);
				}
				else
				{
					$insert_id = $this->accounttype_model->add($typename);
				}
			}
		}
		
		$data['type'] = $this->accounttype_model->get();			
		
		if($tid > 0)
		{
			foreach($data['type'] as $type)
			{
				if($tid == $type->tid)
				{
					$data['info'] = $type;
					break;
				}
			}
		}
		else
		{
			$info = array(
					'tid' => 0,
					'typename' => ''
			);
			
			$data['info'] = (object)$info;
		}
		// 加载视图
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/accounttype', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function deletecategory($tid)
	{
		$tid = intval($tid);
		
		// 判断类别下是否有内容
		$query = $this->accounts_model->get(array('tid' => $tid));
		if($query->num_rows() > 0)
		{
			echo '类别有关联记录';
		}
		else
		{
			$num = $this->accounttype_model->delete($tid);
			if($num > 0)
			{
				echo 'ok';
			}
			else
			{
				echo '类别不存在';
			}
		}
	}
}