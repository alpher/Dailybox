<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/controllers/Notes.php
 * Description CodeIgniter Contoller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/26
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Notes extends Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
	
		$this->load->model('notes_model');
		$this->load->model('notetype_model');
	}
	
	public function index($tid = 0/*, $key = ''*/)
	{
		$data = array();
		$data['title'] = '日记管理';
		
		$this->load->library('Pagination');
		$options = array();

		// 判断是否有提交搜索
		/*
		if($this->input->post('submit') == 1)
		{
			$key = $this->input->post('key');
		}
		*/

		// 构造搜索字段
		$key = $this->input->get('key');
		$key = urldecode($key);
		if(!empty($key))
		{
			$options = array(
					'title' => $key,
					'memo' => $key
			);
		}		
		$data['key'] = $key;

		if(intval($tid) > 0)
		{
			$options['tid'] = intval($tid);
		}
		
		// 获取数据
		$query_temp = $this->notes_model->get($options);
		
		// 配置分页信息
		//$config['base_url'] = site_url('notes/index/' . $tid . '/' . $key);
		$config['base_url'] = site_url('notes/index/' . $tid);
		$config['total_rows'] = $query_temp->num_rows();
		$config['per_page'] = 20;
		$config['uri_segment'] = 4;
		$config['first_link'] = '首页';
		$config['last_link'] = '尾页';
		
		// 格式
		$config['first_tag_open'] =
		$config['last_tag_open'] =
		$config['next_tag_open'] =
		$config['prev_tag_open'] =
		$config['num_tag_open'] = '<li>';
		
		$config['first_tag_close'] =
		$config['last_tag_close'] =
		$config['next_tag_close'] =
		$config['prev_tag_close'] =
		$config['num_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$this->pagination->initialize($config);
		// 传参数给VIEW
		$data['page_links'] = $this->pagination->create_links();
		
		// 分页
		//$offset = $this->uri->segment(5);
		$offset = $this->uri->segment(4);
		$offset = ($offset < 0) ? 0 : $offset;
		$temp = floor($config['total_rows'] / $config['per_page']) * $config['per_page'];
		$offset = ($offset > $temp) ?  $temp : $offset;
		
		// 再次查询，活动要显示的数据
		$options['limit'] = $config['per_page'];
		$options['offset'] = $offset;
		$query = $this->notes_model->get($options);
		
		// 获取类别
		$type = $this->notetype_model->get();
		$temp = $query->result();
		
		$rows = array();
		// 加入类别
		foreach($temp as $row)
		{
			foreach ($type as $t)
			{
				if($row->tid == $t->tid)
				{
					$row->typename = $t->typename;
					break;
				}
			}
				
			$rows[] = $row;
		}
		
		$data['rows'] = $rows;
		$data['typelist'] = $this->notetype_model->get();
		
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/notes', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function edit($id = 0)
	{
		$data = array();
		/*
		$data['css_files'] = array(
			'static/assets/stylesheets/jquery-te-1.4.0.css'
		);
		
		$data['js_files'] = array(
				'static/assets/javascripts/jquery-te-1.4.0.min.js'
		);
		*/
		if($this->input->post('submit') == 1)
		{
			$this->load->library('form_validation');
				
			//构造验证规则
			$rules = array(
					array(
							'field' => 'title',
							'label' => '标题',
							'rules' => 'required',
							'errors' => array(
									'required' => '%s不能为空'
							)
					),
					array(
							'field' => 'tid',
							'label' => '类别',
							'rules' => 'required',
							'errors' => array(
									'required' => '请选择%s'
							)
					),
					array(
							'field' => 'addtime',
							'label' => '日期',
							'rules' => 'required',
							'errors' => array(
									'required' => '%s不能为空'
							)
					)
			);
				
			// 设置验证规则
			$this->form_validation->set_rules($rules);
				
			if($this->form_validation->run() == false)
			{
				$data['message'] = validation_errors();
			}
			else
			{
				$id = intval($this->input->post('id'));
		
				$data['info'] = array();
				
				$data['info']['title'] = $this->input->post('title');
				$data['info']['content'] = $this->input->post('content');
				$data['info']['tid'] = $this->input->post('tid');
				$data['info']['edittime'] = time();
				$addtime = strtotime($this->input->post('addtime'));
				if($addtime > 0)
				{
					$data['info']['addtime'] = $addtime;
				}
				else
				{
					$data['info']['addtime'] = time();
				}
				
				// 判断是否是编辑状态
				if($id > 0)
				{
					$data['info']['id'] = $id;					
	
					$this->notes_model->update($data['info']);
				}
				else
				{
					//$data['info']['addtime'] = time();
						
					$this->notes_model->add($data['info']);
				}
				
				redirect('notes');
			}
		}
		else
		{
			if($id > 0)
			{
				$query = $this->notes_model->get(array('id' => $id, 'limit' => 1));
		
				if($query->num_rows() > 0)
				{
					$row = $query->row_array();
		
					$data['title'] = '编辑日记';
						
					$data['info'] = $row;
				}
				else
				{
					$data['message'] = '记录不存在或已被删除';
				}
			}
			else
			{
				$data['title'] = '添加日记';
		
				$data['info'] = array(
						'id' => 0,
						'title' => '',
						'content' => '',
						'tid' => 0,
						'addtime' => time()
				);
			}
		}
		
		// 类别
		$data['type'] = $this->notetype_model->get();
		
		// 加载视图
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/notes_add', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}

	public function show($id)
	{
		$id = intval($id);
		$query = $this->notes_model->get(array('id' => $id, 'limit' => 1));
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();			
			echo sprintf('<ul><li>%s</li><li>%s</li><li>%s</li></ul>', $row->title, date('Y-m-d',$row->addtime), str_replace("\n", "<br>", $row->content));
		}
		else
		{
			echo 'NULL';
		}
	}
	
	public function delete($id)
	{
		$id = intval($id);
		if($id > 0)
		{
			$num = $this->notes_model->delete(array('id' => $id));
				
			if($num > 0)
			{
				echo 'ok';
			}
			else
			{
				echo '记录不存在';
			}
		}
		else
		{
			echo '缺少参数';
		}
	}
	
	public function category($tid = 0)
	{
		$data['title'] = '日记类别';
		
		$tid = intval($tid);
		
		// 是否有提交表单
		if($this->input->post('submit') == 1)
		{
			$catid = intval($this->input->post('tid'));
			
			$typename = $this->input->post('typename');
			
			if(empty($typename))
			{
				$data['message'] = '类别名称不能为空';
			}
			else
			{
				if($catid > 0)
				{					
					$nums = $this->notetype_model->update($catid, $typename);
				}
				else
				{
					$insert_id = $this->notetype_model->add($typename);
				}
			}
		}
		
		$data['type'] = $this->notetype_model->get();			
		
		if($tid > 0)
		{
			foreach($data['type'] as $type)
			{
				if($tid == $type->tid)
				{
					$data['info'] = $type;
					break;
				}
			}
		}
		else
		{
			$info = array(
					'tid' => 0,
					'typename' => ''
			);
			
			$data['info'] = (object)$info;
		}
		
		// 加载视图
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/notetype', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function deletecategory($tid)
	{
		$tid = intval($tid);
		
		// 判断类别下是否有内容
		$query = $this->notes_model->get(array('tid' => $tid));
		if($query->num_rows() > 0)
		{
			echo '类别有关联记录';
		}
		else
		{
			$num = $this->notetype_model->delete($tid);
			if($num > 0)
			{
				echo 'ok';
			}
			else
			{
				echo '类别不存在';
			}
		}
	}
}