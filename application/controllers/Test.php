<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/controllers/Test.php
 * Description CodeIgniter Contoller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends Front_Controller {
	
	public function __construct()
	{
		parent::__construct();
	
		$this->load->library('encryption');
		$this->load->database();
	}
	
	public function time()
	{
		$start = strtotime('2016-02-07');
		$end = time();
		$fre = 1;
		$fre *= (24 * 3600);
		
		$temp = ($end - $start) / $fre;
		
		echo $temp . '<br>';
		
		while($temp > 1) $temp--;
		
		echo $temp . '<br>';
		
		$last = $end - $temp * $fre;
		
		echo date('Y-m-d H:i:s', $last);
	}
	
	public function alter()
	{
		exit;
		$sql = 'alter table `db_projects` add `frequency` smallint(3) not null default 1';
		
		$this->db->query($sql);
	}
	
	public function code()
	{
		die;
		$str = 'I love php programing!';
		//$key = $this->encryption->create_key(16);
		$key = hex2bin('25547590068b2c0e4fe571e2b45bce88');
	
		$this->encryption->initialize(
				array(
						'key' => $key
				)
		);
				
	
		$encode = $this->encryption->encrypt($str);
	
		$decode = $this->encryption->decrypt($encode);
	
		echo sprintf('<p>str = %s</p>', $str);
		
		echo sprintf('<p>key = %s, len = %d</p>', bin2hex($key), strlen(bin2hex($key)));
	
		echo sprintf('<p>encode = %s, len = %d</p>', $encode, strlen($encode));
	
		echo sprintf('<p>decode = %s</p>', $decode);
	}
	
	public function obj()
	{
		die;
		$data = array(
				'one' => '1',
				'two' => '2',
				'three' => 3
		);
		
		$obj = (object)$data;
		
		echo '<pre>';
		//print_r($data);
		var_dump($data);
		
		echo '<hr>';
		//print_r($obj);
		var_dump($obj);
		
		echo $obj->one;
		echo '</pre>';
	}
}