<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/controllers/Events.php
 * Description CodeIgniter Contoller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/25
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('events_model');
	}
	
	public function index($key = '关键字')
	{
		$data = array();
		$data['title'] = '日志管理';
		
		$this->load->library('Pagination');	
		
		// 判断是否有提交搜索
		if($this->input->post('submit') == 1)
		{
			$key = $this->input->post('key');
		}
		
		$options = array();
		
		// 构造搜索字段
		$key = urldecode($key);
		if(!empty($key) && $key != '关键字')
		{
			$options = array(
					'title' => $key,
					'content' => $key,
					'ip' => $key
			);
		}
		
		$data['key'] = $key;
		
		// 获取数据
		$query_temp = $this->events_model->get($options);
		
		// 配置分页信息
		$config['base_url'] = site_url('events/index/' . $key);
		$config['total_rows'] = $query_temp->num_rows();
		$config['per_page'] = 20;
		$config['uri_segment'] = 4;
		$config['first_link'] = '首页';
		$config['last_link'] = '尾页';
		
		// 格式
		$config['first_tag_open'] = 
			$config['last_tag_open'] = 
			$config['next_tag_open'] =
			$config['prev_tag_open'] =
			$config['num_tag_open'] = '<li>';
		
		$config['first_tag_close'] = 
			$config['last_tag_close'] =
			$config['next_tag_close'] =
			$config['prev_tag_close'] =
			$config['num_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$this->pagination->initialize($config);
		// 传参数给VIEW
		$data['page_links'] = $this->pagination->create_links();
		
		// 分页
		$offset = $this->uri->segment(4);
		$offset = ($offset < 0) ? 0 : $offset;
		$temp = floor($config['total_rows'] / $config['per_page']) * $config['per_page'];
		$offset = ($offset > $temp) ?  $temp : $offset;
		
		// 再次查询，活动要显示的数据
		$options['limit'] = $config['per_page'];
		$options['offset'] = $offset;
		$query = $this->events_model->get($options);
		$data['evts'] = $query->result();
		
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/eventlist', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	function delete($id = '')
	{
		$this->events_model->delete($id);
		
		$this->index();
	}
	
	function ajax_delete($id)
	{
		$nums = $this->events_model->delete($id);
		
		if($nums > 0)
		{
			echo 'ok';
		}
		else 
		{
			echo '没有可删除的日志';
		}
	}
}