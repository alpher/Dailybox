<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/controllers/Attachments.php
 * Description CodeIgniter Contoller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Attachments extends Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
	
		$this->load->model('attachments_model');
	}
	
	public function index(/*$key = '关键字'*/)
	{
		$data = array();
		$data['title'] = '附件管理';
		
		$this->load->library('Pagination');
		
		// 判断是否有提交搜索
		/*
		if($this->input->post('submit') == 1)
		{
			$key = $this->input->post('key');
		}*/
		$key = $this->input->get('key');
		$options = array();
		
		// 构造搜索字段
		$key = urldecode($key);
		if(!empty($key) && $key != '关键字')
		{
			$options = array(
					'title' => $key
			);
		}
		
		$data['key'] = $key;
		
		// 获取数据
		$query_temp = $this->attachments_model->get($options);
		
		// 配置分页信息
		//$config['base_url'] = site_url('attachments/index/' . $key);
		$config['base_url'] = site_url('attachments/index');
		$config['total_rows'] = $query_temp->num_rows();
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;
		$config['first_link'] = '首页';
		$config['last_link'] = '尾页';
		
		// 格式
		$config['first_tag_open'] =
		$config['last_tag_open'] =
		$config['next_tag_open'] =
		$config['prev_tag_open'] =
		$config['num_tag_open'] = '<li>';
		
		$config['first_tag_close'] =
		$config['last_tag_close'] =
		$config['next_tag_close'] =
		$config['prev_tag_close'] =
		$config['num_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$this->pagination->initialize($config);
		// 传参数给VIEW
		$data['page_links'] = $this->pagination->create_links();
		
		// 分页
		$offset = $this->uri->segment(3);
		$offset = ($offset < 0) ? 0 : $offset;
		$temp = floor($config['total_rows'] / $config['per_page']) * $config['per_page'];
		$offset = ($offset > $temp) ?  $temp : $offset;
		
		// 再次查询，活动要显示的数据
		$options['limit'] = $config['per_page'];
		$options['offset'] = $offset;
		$query = $this->attachments_model->get($options);
		$data['rows'] = $query->result();
		
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/attachments', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function image($aid)
	{
		if($aid > 0)
		{
			$query = $this->attachments_model->get(array('aid' => $aid, 'limit' => 1));
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				
				header("Content-Type: " . $row->mimetype);         
        if(!in_array($row->extname, array('.gif', '.jpg', '.png', '.jpeg', '.pdf'))) {
          header("Accept-Ranges: bytes");
          header("Accept-Length: " . count($row->contents));
          header("Content-Disposition: attachment; filename=" . $row->title . $row->extname);
        }
        
				echo $row->contents;
			}
		}
	}
		
	public function edit($aid = 0)
	{
		$data = array();
		
		if($this->input->post('submit') == 1)
		{			
			// 配置文件上传类
			$config = array(
					'upload_path' => './static/upload/',
					'allowed_types' => 'gif|jpg|png|jpeg|bmp|pdf|doc|docx|txt|xls|xlsx|zip|rar|gz'
			);
			
			// 加载文件上传类
			$this->load->library('upload', $config);
			//$this->load->helper('file');
						
			$aid = intval($this->input->post('aid'));
			
			$data['attach'] = array();
			// 判断是否是编辑状态
			if($aid > 0)
			{
				$query = $this->attachments_model->get(array('aid' => $aid, 'limit' => 1));
				
				if($query->num_rows() > 0)
				{
					$row = $query->row();
					$data['attach'] = array(
							'aid' => $row->aid,
							'title' => $row->title,
							'contents' => $row->contents,
							'mimetype' => $row->mimetype,
							'extname' => $row->extname,
							'addtime' => $row->addtime,
							'edittime' => $row->edittime
					);
				}
			}
			
			$title = $this->input->post('title');
			if(!empty($title))
			{
				$data['attach']['title'] = $title;
			}
			
			// 处理上传文件
			if($this->upload->do_upload('fileurl') == false)
			{
				$data['title'] = '添加附件';
				
				$title = $this->input->post('title');
				// 只修改标题
				if($aid > 0 && !empty($title))
				{
					$data['attach']['edittime'] = time();
					
					$this->attachments_model->update($data['attach']);
					
					redirect('attachments');
				}
				else 
				{
					$data['attach']['title'] = '';
					$data['attach']['aid'] = 0;
					
					$data['message'] = $this->upload->display_errors();
				}
			}
			else 
			{
				//$data['attach']['title'] = $this->input->post('title');
				//$data['attach']['aid'] = intval($this->input->post('aid'));
				
				// 操作附件
				$uploads = $this->upload->data();
				
				// 将附件读入内存
				$data['attach']['contents'] = file_get_contents($uploads['full_path']);
				$data['attach']['mimetype'] = $uploads['file_type'];
				$data['attach']['extname'] = $uploads['file_ext'];
				$data['attach']['edittime'] = time();
				
				$title = $this->input->post('title');
				if(empty($title))
				{
					$data['attach']['title'] = $uploads['raw_name'];
				}
				
				if($aid > 0)
				{
					$data['attach']['aid'] = $aid;
					
					$this->attachments_model->update($data['attach']);
				}
				else 
				{
					$data['attach']['addtime'] = time();
					//print_r($data['attach']);die;
					
					$this->attachments_model->add($data['attach']);
				}
				
				// 删除临时文件
				unlink($uploads['full_path']);
				
				redirect('attachments');
			}
		}
		else
		{
			if($aid > 0)
			{
				$query = $this->attachments_model->get(array('aid' => $aid, 'limit' => 1));
				if($query->num_rows() > 0)
				{
					$row = $query->row(0);
					
					$data['title'] = '编辑附件';
					$data['attach'] = array(
						'title' => $row->title,
						'aid' => $row->aid
					);
				}
				else 
				{
					$data['message'] = '附件不存在或已被删除';
				}
			}
			else 
			{
				$data['title'] = '添加附件';
				
				$data['attach'] = array(
					'title' => '',
					'aid' => 0
				);
			}
		}
		
		// 加载视图
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/attachments_add', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function delete($aid)
	{
		$aid = intval($aid);
		
		if($aid <= 0)
		{
			echo '错误';
			return;
		}
		//echo 'ok';return;
		
		$data = array('aid' => $aid);
		
		$num = $this->attachments_model->delete($data);
		
		if($num > 0)
		{
			echo 'ok'; 
		}
		else 
		{
			echo '失败';
		}
	}	
	
}