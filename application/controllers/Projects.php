<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/controllers/Projects.php
 * Description CodeIgniter Contoller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
	
		$this->load->model('projects_model');
		$this->load->model('items_model');
		$this->load->model('tracks_model');
	}
		
	public function index(/*$key = '关键字'*/)
	{
		$data = array();
		$data['title'] = '计划管理';
		$options = array();
		$this->load->library('Pagination');
		
		/*
		// 判断是否有提交搜索
		if($this->input->post('submit') == 1)
		{
			$key = $this->input->post('key');
		}
		*/		
		// 构造搜索字段
		$key = $this->input->get('key');
		$key = urldecode($key);
		if(!empty($key) && $key != '关键字')
		{
			$options = array(
					'project' => $key,
					'memo' => $key
			);
		}
		
		$data['key'] = $key;

		// 获取数据
		$query_temp = $this->projects_model->get($options);
		
		// 配置分页信息
		//$config['base_url'] = site_url('projects/index/' . $key);
		$config['base_url'] = site_url('projects/index');
		$config['total_rows'] = $query_temp->num_rows();
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;
		$config['first_link'] = '首页';
		$config['last_link'] = '尾页';
		
		// 格式
		$config['first_tag_open'] =
		$config['last_tag_open'] =
		$config['next_tag_open'] =
		$config['prev_tag_open'] =
		$config['num_tag_open'] = '<li>';
		
		$config['first_tag_close'] =
		$config['last_tag_close'] =
		$config['next_tag_close'] =
		$config['prev_tag_close'] =
		$config['num_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$this->pagination->initialize($config);
		// 传参数给VIEW
		$data['page_links'] = $this->pagination->create_links();
		
		// 分页
		$offset = $this->uri->segment(3);
		$offset = ($offset < 0) ? 0 : $offset;
		$temp = floor($config['total_rows'] / $config['per_page']) * $config['per_page'];
		$offset = ($offset > $temp) ?  $temp : $offset;
		
		// 再次查询，活动要显示的数据
		$options['limit'] = $config['per_page'];
		$options['offset'] = $offset;
		$query = $this->projects_model->get($options);
		
		$rows = $query->result();
		
		$data['rows'] = $rows;
		
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/projects', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function edit($proid = 0)
	{
		$data = array();
		
		//$data['css_files'] = array('static/assets/stylesheets/jquery.simple-dtpicker.css');
		//$data['js_files'] = array('static/assets/javascripts/jquery.simple-dtpicker.js');
		
		if($this->input->post('submit') == 1)
		{
			$this->load->library('form_validation');
				
			//构造验证规则
			$rules = array(
					array(
							'field' => 'project',
							'label' => '计划名',
							'rules' => 'required',
							'errors' => array(
									'required' => '%s不能为空'
							)
					),
					array(
							'field' => 'frequency',
							'label' => '频率',
							'rules' => 'required',
							'errors' => array(
									'required' => '%s不能为空'
							)
					),
					array(
							'field' => 'addtime',
							'label' => '开始日期',
							'rules' => 'required',
							'errors' => array(
									'required' => '%s不能为空'
							)
					),
					array(
							'field' => 'endtime',
							'label' => '结束日期',
							'rules' => 'required',
							'errors' => array(
									'required' => '%s不能为空'
							)
					)
			);
				
			// 设置验证规则
			$this->form_validation->set_rules($rules);
				
			if($this->form_validation->run() == false)
			{
				$data['message'] = validation_errors();
			}
			else
			{
				$proid = intval($this->input->post('proid'));
		
				$data['info'] = array();
				
				$data['info']['project'] = $this->input->post('project');
				$data['info']['memo'] = $this->input->post('memo');
				$data['info']['frequency'] = $this->input->post('frequency');
				$data['info']['score'] = floatval($this->input->post('score'));
				$data['info']['addtime'] = strtotime($this->input->post('addtime'));
				$data['info']['endtime'] = strtotime($this->input->post('endtime'));
				
				// 判断是否是编辑状态
				if($proid > 0)
				{
					$data['info']['proid'] = $proid;					
	
					$this->projects_model->update($data['info']);
				}
				else
				{					
					$data['info']['uid'] = $this->data['uid'];
						
					$this->projects_model->add($data['info']);
				}
				
				redirect('projects');
			}
		}
		else
		{
			if($proid > 0)
			{
				$query = $this->projects_model->get(array('proid' => $proid, 'limit' => 1));
		
				if($query->num_rows() > 0)
				{
					$row = $query->row_array();
		
					$data['title'] = '编辑计划';
	
					$row['addtime'] = date('Y-m-d', $row['addtime']);
					$row['endtime'] = date('Y-m-d', $row['endtime']);
					$data['info'] = $row;
				}
				else
				{
					$data['message'] = '记录不存在或已被删除';
				}
			}
			else
			{
				$data['title'] = '添加计划';
		
				$data['info'] = array(
						'proid' => 0,
						'project' => '',
						'score' => 0,
						'memo' => '',
						'frequency' => 1,
						'addtime' => '',
						'endtime' => ''
				);
			}
		}
		
		// 加载视图
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/projects_add', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function delete($proid)
	{
		$proid = intval($proid);
		if($proid > 0)
		{
			$num = $this->projects_model->delete(array('proid' => $proid));
				
			if($num > 0)
			{
				echo 'ok';
			}
			else
			{
				echo '记录不存在';
			}
		}
		else
		{
			echo '缺少参数';
		}
	}
	
	public function items($proid)
	{
		$data['title'] = '计划条目';
	
		$proid = intval($proid);
		
		$data['proid'] = $proid;
	
		$rows = $this->items_model->get(array('proid' => $proid));
	
		$data['rows'] = $rows;
	
		// 加载视图
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/items', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function additems($proid, $itemid = 0)
	{
		$data['title'] = '条目管理';
		if(empty($proid))
		{
			$data['message'] = '缺少参数';
		}
		$data['proid'] = intval($proid);
		
		$fields = array('project');
		$query = $this->projects_model->get(array('fields' => $fields, 'proid' => $proid, 'limit' => 1));
		$obj = $query->row();
		$data['project'] = $obj->project;
		
		// 判断是否有提交表单
		if($this->input->post('submit') == '1')
		{
			$itemid = intval($this->input->post('itemid'));			
			$item = $this->input->post('item');
			$sortrank = is_numeric($this->input->post('sortrank')) ? intval($this->input->post('sortrank')) : 5;
			
			if(empty($item))
			{
				$data['message'] = '条目名称不能为空';
			}
			else
			{
				// 判断是编辑还是新增
				if($itemid > 0)
				{
					$options = array(
							'itemid' => $itemid,
							'item' => $item,
							'sortrank' => $sortrank
					);
					
					$this->items_model->update($options);
					
					redirect('projects/items/' . $proid);
				}
				else 
				{
					$options = array(
							'proid' => $proid,
							'item' => $item,
							'sortrank' => $sortrank
					);
					
					$this->items_model->add($options);
					
					redirect('projects/items/' . $proid);
				}
			}
		}
		else 
		{
			$itemid = intval($itemid);
			
			// 判断是编辑还是新增
			if($itemid > 0)
			{
				$rows = $this->items_model->get(array('itemid' => $itemid, 'limit' => 1));
				
				$data['info'] = $rows[0];
				
				$data['title'] = '修改条目';
			}
			else
			{
				$row = array(
						'itemid' => 0,
						'proid' => $proid,
						'item' => '',
						'sortrank' => 5
				);
				
				$data['info'] = (object)$row;
				
				$data['title'] = '增加条目';
			}
		}
		
		// 加载视图
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/items_add', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	public function deleteitems($itemid)
	{
		$itemid = intval($itemid);
		
		if($itemid <= 0)
		{
			echo '缺少参数';
			return;
		}
		else 
		{		
			$nums = $this->items_model->delete(array('itemid' => $itemid));
			
			if($nums > 0)
			{
				echo 'ok';
			}
			else				
			{
				echo '失败';
			}
		}
	}
	
 	public function tracks($proid)
 	{
 		$data['title'] = '计划跟踪';
 		
 		$proid = intval($proid);
 		
 		$data['proid'] = $proid;
 		
 		// 列出计划所有条目
 		$fields = array('project', 'addtime', 'frequency');
 		$query = $this->projects_model->get(array('fields' => $fields, 'proid' => $proid, 'limit' => 1));
 		$obj = $query->row();
 		$data['project'] = $obj->project;
 		
 		// 当前日期
 		$cur_time = time();
 		
 		// 获得项目周期的当前周期起始日期
 		$start_time = $this->getTrackTime($obj->addtime, $cur_time, $obj->frequency);
 		 		
 		$rows = $this->items_model->get(array('proid' => $proid));
 		 		 		
  		// 构造itemid数组
  		$itemids = array();
  		foreach ($rows as $r)
  		{
  			$itemids[] = $r->itemid;
  		}
  		
  		// 构造获取跟踪记录所需参数
  		$options = array(
  				'itemids' => $itemids,
  				'start' => $start_time,
  				'end' => $cur_time
  		);
  		
  		$tracks = $this->tracks_model->get_tracks($options);
  		
  		// 处理计划项目的实施情况
  		foreach ($rows as $r)
  		{
  			// 付初始状态
  			$r->state = 0;
  			
  			// 完全没有实施
  			if($tracks == false)
  			{  				
  				continue;
  			}
  			
  			foreach($tracks as $t)
  			{
  				if($r->itemid == $t->itemid)
  				{
  					$r->state = 1;
  					$r->date = date('y-n-j H:i', $t->notetime);
  					continue;
  				}
  			}
  		}
  		
  		// 发送结果
  		$data['rows'] = $rows;
 		
 		// 加载视图
 		$this->load->view($this->theme.'/chip/header', $data);
 		$this->load->view($this->theme.'/tracks', $data);
 		$this->load->view($this->theme.'/chip/footer', $data);
 	}
 	
 	public function record($itemid)
 	{
 		$itemid = intval($itemid);
 		
 		$state = $this->tracks_model->set_tracks($itemid);
 		
 		echo $state ? 'ok' : 'fail';
 	}
}