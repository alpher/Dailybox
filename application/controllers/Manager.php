<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/controllers/Manager.php
 * Description CodeIgniter Contoller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/13
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Manager extends Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('projects_model');
		$this->load->model('items_model');
		$this->load->model('tracks_model');
	}
	
	public function index()
	{
		$data['title'] = "系统登录";
		
		// 加载当日需完成的计划
		$tm = time();
		$projects = $this->projects_model->get_availables($tm);
		$proids = array();
		if($projects) 
		{
			// 记录项目日期
			$prj_time = array();
			
			foreach($projects as $p)
			{
				// 构造计划ID数组
				$proids[] = $p->proid;
				
				// 获得当前项目周期的当前周期起始日期
				$start_time = $this->getTrackTime($p->addtime, $tm, $p->frequency);
				
				// 项目与计算日期关联
				$prj_time[$p->proid] = array(
						'proid' => $p->proid,
						'name' => $p->project,
						'start' => $start_time,
						'end'	=> $tm
				);
			}
			
			// 获取已知计划的条目
			$items = $this->items_model->get_in_projects($proids);
			$itemids = array();
						
			if($items)
			{
				// 构造条目ID数组
				$prj_items = array();
				
				foreach($items as $itm)
				{
					$prj_items[$itm->proid]['time'] = $prj_time[$itm->proid];
					$prj_items[$itm->proid]['itemids'][] = $itm->itemid;
					
					//$itemids[] = $itm->itemid;
				}
				
				// 获得各个计划中当前已经完成的条目
				$tracks = array();
				foreach($prj_items as $k => $v)
				{
					$options = array(
							'itemids' => $v['itemids'],
							'start' => $v['time']['start'],
							'end' => $v['time']['end']
					);
					
					// 分项目获得记录实施情况
					$tracks_temp = $this->tracks_model->get_tracks($options);
					
					// 合并所有记录
					if($tracks_temp)
					{
						foreach($tracks_temp as $t)
						{
							$tracks[] = $t;
						}
					}
				}
				
				// 处理计划项目的实施情况
				foreach ($items as $itm)
				{
					// 付初始状态
					$itm->state = 0;
					
					// 获得当前项目的记录周期起始时间
						
					// 完全没有实施
					if(empty($tracks))
					{
						continue;
					}
						
					foreach($tracks as $t)
					{
						if($itm->itemid == $t->itemid)
						{
							$itm->state = 1;
							$itm->date = date('y-n-j H:i', $t->notetime);
							
							continue;
						}
					}
				}
				
				$list = array();
				// 综合计划处理
				foreach($projects as $p)
				{
					$list[$p->proid]['project'] = $p->project;
					$list[$p->proid]['items'] = array();
					
					// 计划条目处理
					foreach ($items as $item)
					{
						if($item->proid == $p->proid)
						{
							$list[$p->proid]['items'][] = $item;
						}
					}
				}
				
				$data['projects'] = $list;
				
				//echo '<pre>';
				//print_r($list);die;
			}
		}
		
		
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/manager', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
}