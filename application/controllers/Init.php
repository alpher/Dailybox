<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/controllers/Init.php
 * Description CodeIgniter Contoller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/23
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Init extends Front_Controller {
	
	public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * 判断登录是否成功
     * 
     * @return array
     * 
     */
    private function loginCheck()
    {
    	$this->load->model('users_model');
    	$this->load->library('form_validation');
    	
    	//$state = false;
    	$message = array(
    			'state' => false,
    			'message' => '',
    			'user' => array()
    	);

		if($this->input->post('submit') == 1)
		{
			// 验证规则
			$rules = array(
					array(
							'field' => 'username',
							'label' => '用户名或Email',
							'rules' => 'required|min_length[6]',
							'errors' => array(
									'required' => '%s不能为空',
									'min_length' => '%s至少需要6个字符'
							)
					),
					array(
							'field' => 'password',
							'label' => '密码',
							'rules' => 'required|min_length[6]',
							'errors' => array(
									'required' => '%s不能为空',
									'min_length' => '%s至少需要6个字符'
							)
					)
			);

			// 绑定验证规则
    		$this->form_validation->set_rules($rules);
			
			// 验证不通过
			if($this->form_validation->run() == false)
			{
				$message['state'] = false;
				$message['message'] = validation_errors();

				return $message;
			}

			$username = $this->input->post('username', true);
			$password = $this->input->post('password', true);
		}
		else
		{
			// 验证不通过
			if($this->input->get('callback') == '')
			{
				$message['state'] = false;
				$message['message'] = '方法不对';

				return $message;
			}

			$username = $this->input->get('username', true);
			$password = $this->input->get('password', true);
		}

		// 校验用户
		$user = $this->users_model->get(array('username' => $username, 'email' => $username));
			
		if($user)
		{
			//$pwd = md5(md5($password).$user[0]->salt);
			$pwd = md5($password.$user[0]->salt);
			// 对比密码
			if($user[0]->pwd == $pwd)
			{
				//保存会话信息
				$this->session->set_userdata('uid', $user[0]->uid);
				$this->session->set_userdata('username', $user[0]->username);
				$this->session->set_userdata('islogin', true);
				
				// 记录日志
				$content = sprintf('用户名：%s', $username);
				$this->events('登录成功', $content);
	
				$message['state'] = true;
				$message['message'] = '登录成功';
				$message['user'] = array(
						'uid' => $user[0]->uid,
						'username' => $user[0]->username
				);
			}
			else
			{
				$message['state'] = false;
				$message['message'] = '密码错误';
	
				$content = sprintf('用户名：%s，测试密码:%s', $username, $password);
				$this->events('密码错误', $content);
			}
		}
		else
		{
			$message['state'] = false;
			$message['message'] = '用户不存在';
				
			$content = sprintf('用户名：%s，测试密码:%s', $username, $password);
			$this->events('用户不存在', $content);
		}
    	
    	return $message;
    }
    
    /**
     * ajax登录，支持跨域
     */
    public function ajaxLogin()
    {
    	$data = array(
    			'state' => '0',
    			'message' => '登录失败'
    	);
    	

		$callback = $this->input->get('callback');
		
		$info = $this->loginCheck();

		//var_dump($info);die;
		
		if($info['state'])
		{
			$data['state'] = 1;
			$data['message'] = $info['user']['username'];
		}
		
		$data['message'] = $info['message'];

    	
    	// 生成JSON格式
    	$json = json_encode($data);
    	
		if(!empty($callback))
		{
    		// 输出JSONP消息
    		echo sprintf('%s(%s)', $callback, $json);
		}
		else
		{
			$this->output->set_content_type('application/json')
						 ->set_output($json);
		}
    }
    
    /**
     * 桌面端登陆
     */
	public function index($message = '')
	{		
		$data['title'] = "系统登录";

		$data['css_files'] = array('signin.css');
		
		if(!empty($message))
		{
			$data['message'] = urldecode($message);
		}
		
		if($this->input->post('submit') == 1)
		{
			$info = $this->loginCheck();
			
			if($info['state'])
			{
				redirect('manager');
			} 
			else 
			{
				$data['message'] = $info['message'];
			}
		}
		
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/login', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
}
