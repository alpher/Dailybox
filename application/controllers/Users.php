<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/controllers/Users.php
 * Description CodeIgniter Contoller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/06
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller {
	
	public function __construct()
    {
        parent::__construct(); 
    }
    
	public function index($key = '')
	{
		$data = array();
		$data['title'] = "用户管理 ";
		
		$this->load->model('users_model');
		
		// 判断是否有提交搜索
		if($this->input->post('submit') == 1)
		{
			$key = $this->input->post('key');
		}
		
		$options = array();
		
		// 构造搜索字段
		if(!empty($key))
		{
			$options = array(
				'username' => $key,
				'email' => $key
			);
		}
		
		$rows = $this->users_model->get($options);
		
		$data['users'] = $rows;
		
		// 重现表单
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/userslist', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
		
	public function editpwd($uid = '')
	{		
		$data = array();
		
		$data['title'] = "修改密码";
		
		$this->load->model('users_model');
		$this->load->library('form_validation');
		
		$submit = $this->input->post('submit', true);
		
		// 找出用户数据
		$uid = empty($uid) ? $this->data['uid'] : intval($uid);
		$user = $this->users_model->get(array('uid' => $uid));
		//会话失效，用户不存在
		if($user == false)
		{
			header('Location: '.$this->app_url.'/init/index/'.urlencode('尚未登录或登录已失效'));
			return;
		}
		
		if($submit != 1)
		{
			$data['username'] = $user[0]->username;
			$data['email'] = $user[0]->email;			
		}
		else 
		{
			//构造验证规则
			$rules = array(
					array(
							'field' => 'oldpassword',
							'label' => '原密码',
							'rules' => 'required|min_length[6]',
							'errors' => array(
									'required' => '%s正确才能修改信息',
									'min_length' => '%s至少需要6个字符'
							)
					),
					array(
							'field' => 'password',
							'label' => '新密码',
							'rules' => 'min_length[6]',
							'errors' => array(
									'min_length' => '%s至少需要6个字符'
							)
					),
					array(
							'field' => 'password1',
							'label' => '确认密码',
							'rules' => 'min_length[6]|matches[password]',
							'errors' => array(
									'min_length' => '%s至少需要6个字符',
									'matches' => '两次输入的密码不一致'
							)
					),
					array(
							'field' => 'email',
							'label' => 'E-mail',
							'rules' => 'required|valid_email',
							'errors' => array(
									'required' => '%s必须填写',
									'valid_email' => '不是有效的%s'
							)
					)
			);
			
			// 设置验证规则
			$this->form_validation->set_rules($rules);
			
			$data['username'] = $this->data['username'];
			$data['oldpassword'] = $this->input->post('oldpassword', true);
			$data['password'] = $this->input->post('password', true);
			$data['password1'] = $this->input->post('password1', true);
			$data['email'] = $this->input->post('email', true);
			
			// 判断验证状态
			if($this->form_validation->run() != false)
			{
				// 保存提交的信息
				$pwd = md5(md5($data['oldpassword']) . $user[0]->salt);
				//原始密码正确
				if($user[0]->pwd == $pwd)
				{
					//修改用户信息
					$userinfo = array(
							'uid' => $user[0]->uid,
							'email' => $data['email']
					);
						
					if(!empty($data['password']))
					{
						$salt = random_string('alnum', 8);
						$newpwd = md5(md5($data['password']) . $salt);
				
						$userinfo['salt'] = $salt;
						$userinfo['pwd'] = $newpwd;
					}
				
					$num = $this->users_model->update($userinfo);
					if($num > 0)
					{
						if(!empty($data['password']))
						{
							$this->logout();
							return;
						}
				
						$data['message'] = '修改信息成功';
						
						$content = sprintf('修改信息成功, 用户名：%s', $data['username']);
						$this->events('修改信息成功', $content);
					}
					else
					{
						$data['message'] = '错误，修改信息失败';
						
						$content = sprintf('数据保存失败, 用户名：%s', $data['username']);
						$this->events('修改信息失败', $content);
					}
				}
				else 
				{
					$data['message'] = '原密码不正确';
					
					$content = sprintf('原密码不正确, 用户名：%s', $data['username']);
					$this->events('修改信息失败', $content);
				}
			}
		}
		
		// 重现表单
		$this->load->view($this->theme.'/chip/header', $data);
		$this->load->view($this->theme.'/editpwd', $data);
		$this->load->view($this->theme.'/chip/footer', $data);
	}
	
	/**
	 * 注销会话
	 */
	function logout()
	{
		$username = $this->session->userdata('username');
		
		$content = sprintf('用户名：%s', $username);
		$this->events('注销登录', $content);
		
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('islogin');
		$this->session->sess_destroy();
		
		header('Location: '.$this->app_url);
	}
	
	/*
	public function add()
	{
		$this->load->helper('string');
		$this->load->model('users_model');
		
		$salt = random_string('alnum', 8);
		$pwd = md5(md5('654123') . $salt);
		
		$data = array(
			'username' => 'scriptfan',
			'pwd' => $pwd,
			'salt' => $salt, 
			'email' => 'scriptfan@qq.com'
		);
		
		$exists = $this->users_model->get($data);
		if($exists == false)
		{
			$id = $this->users_model->add($data);
			
			if($id > 0)
			{
				echo '添加成功！';
			}
			else
			{
				echo '添加失败！';
			}
		}
		else
		{
			echo '用户已存在';
		}		
	}*/
}
