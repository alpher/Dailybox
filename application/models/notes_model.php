<?php
/**
 * DailyBox
 * Version 1.0.1
 * File notes_model.php
 * Description CodeIgniter Model
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/26
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Notes_model extends My_Model {
	
	function __construct()
	{
		parent::__construct();
	
		$this->table = 'notes';
		$this->fields = array('tid', 'title', 'content', 'addtime', 'edittime');
		$this->primary = 'id';
	}
	
	/**
	 * 查询数据
	 *
	 * @param array $options
	 * @return resource
	 */
	function get($options = array())
	{
		$fields = $this->_default($this->fields, array($this->primary));
		$this->db->select($fields);
		
		// OR查询条件
		$condition = '';
		$qualificationArray = array('title', 'content');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				if(empty($condition))
				{
					$condition .=  " `{$qualifier}` like '%" . $this->db->escape_like_str($options[$qualifier]) . "%' ";
				}
				else 
				{
					$condition .=  " OR `{$qualifier}` like '%" . $this->db->escape_like_str($options[$qualifier]) . "%' ";
				}
			}			
		}		

		if(!empty($condition))
		{
			$this->db->where('(' . $condition . ')');
		}
		
		// AND查询语句
		if(isset($options['tid']) && !empty($options['tid']))
		{
			$this->db->where('tid', $options['tid']);
		}

		if(isset($options[$this->primary]))
		{
			$this->db->where($this->primary, $options[$this->primary]);
		}
		
		// 设置排序
		$this->db->order_by($this->primary, 'desc');
		
		// 设置分页和返回记录数量
		if(isset($options['limit']) && isset($options['offset']))
		{
			$this->db->limit($options['limit'], $options['offset']);
		}
		else if(isset($options['limit']))
		{
			$this->db->limit($options['limit']);
		}
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		return $query;
	}
	
	/**
	 * 增加记录
	 *
	 * @param array $options
	 * @return int
	 */
	function add($options = array())
	{
		// 判断是否包含必须字段
		if($this->_required(array('title', 'content'), $options) == false) return false;
		
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 执行查询
		$this->db->insert($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->insert_id();
	}
	
	/**
	 *  更新记录
	 *
	 * @param array $options
	 * @return int
	 */
	function update($options = array())
	{
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 设置更新条件
		$this->db->where($this->primary, $options[$this->primary]);
		
		// 执行查询
		$this->db->update($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
	
	/**
	 * 删除记录 
	 * 
	 * @param array $options
	 * @return int
	 */
	function delete($options = array())
	{
		// 设置字段
		$qualificationArray = $this->_default($this->fields, array($this->primary));
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->where($qualifier, $options[$qualifier]);
			}
		}
		
		// 提交查询
		$this->db->delete($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
}