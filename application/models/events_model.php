<?php
/**
 * DailyBox
 * Version 1.0.1
 * File events_model.php
 * Description CodeIgniter Model
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/11
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Events_model extends My_Model {
	
	function __construct()
	{
		parent::__construct();
	
		$this->table = 'events';
		$this->fields = array('title', 'content', 'ip', 'addtime');
		$this->primary = 'id';
	}
	
	/**
	 * 查询数据 
	 * 
	 * @param array $options
	 * @return resource
	 */
	function get($options = array())
	{
		// 设置要显示的字段
		$fields = $this->_default($this->fields, array('id'));
		
		$this->db->select($fields);
		
		// 设置查询条件
		$qualificationArray = array('title', 'content', 'ip');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->or_like($qualifier, $options[$qualifier]);
			}
		}
		
		// 设置排序
		$this->db->order_by('id', 'desc');
		
		// 设置分页和返回记录数量
		if(isset($options['limit']) && isset($options['offset']))
		{
			$this->db->limit($options['limit'], $options['offset']);
		}
		else if(isset($options['limit']))
		{
			$this->db->limit($options['limit']);
		}
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		return $query;
	}
	
	/**
	 * 写日志
	 * 
	 * @param array $options
	 * @return int
	 */
	function add($options = array())
	{
		// 判断是否包含必须字段
		if($this->_required(array('title', 'content'), $options) == false) return false;
				
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 执行查询
		$this->db->insert($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->insert_id();
	}
	
	/**
	 * 清空3天之前的日志
	 * 
	 * @param string $addtime
	 * @return int
	 */
	function delete($id = 0, $addtime = 0)
	{
		// 3天之前的时间 
		$tm = time() - 3*24*3600;
		
		// 如果提交的时间大于规定的最大时间
		if($addtime < $tm)
		{
			$this->db->where('addtime <', $addtime);
		}
		else 
		{
			$this->db->where('addtime <', $tm);
		}
		
		if($id > 0)
		{
			$this->db->where('id', $id);
		}
		
		$this->db->delete($this->table);
		
		return $this->db->affected_rows();
	}
}