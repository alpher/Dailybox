<?php
/**
 * DailyBox
 * Version 1.0.1
 * File accounttype_model.php
 * Description CodeIgniter Model
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounttype_model extends My_Model {
	
	function __construct()
	{
		parent::__construct();
	
		$this->table = 'accounttype';
		$this->fields = array('typename');
		$this->primary = 'tid';
	}
	
	/**
	 * 列表
	 *
	 * @param array $options
	 * @return array
	 */
	function get($options = array())
	{
		// 设置要显示的字段
		$fields = $this->_default($this->fields, array($this->primary));
	
		$this->db->select($fields);
	
		// 设置查询条件
		$qualificationArray = array('tid', 'typename');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->where($qualifier, $options[$qualifier]);
			}
		}
	
		// 设置排序
		$this->db->order_by($this->primary, 'asc');
	
		// 提交查询
		$query = $this->db->get($this->table);
		
		// 判断返回记录数
		if($query->num_rows() == 0) return false;
	
		return $query->result();
	}
	
	/**
	 * 新增
	 *
	 * @param string $typename
	 * @return int
	 */
	function add($typename)
	{
		if(isset($typename) == false)
		{
			return false;
		}
		
		// 给字段赋值
		$this->db->set('typename', $typename);
	
		// 执行查询
		$this->db->insert($this->table);
	
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->insert_id();
	}
	
	/**
	 * 修改
	 *
	 * @param int $tid
	 * @param string $typename
	 * @return int
	 */
	function update($tid, $typename)
	{
		// 给字段赋值
		$this->db->set('typename', $typename);
		
		$this->db->where('tid', $tid);
	
		// 执行查询
		$this->db->update($this->table);
	
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
	
	/**
	 * 删除数据
	 * 
	 * @param int $tid
	 * @return int
	 */
	function delete($tid)
	{
		$this->db->where('tid', $tid);
		
		// 提交查询
		$this->db->delete($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
}