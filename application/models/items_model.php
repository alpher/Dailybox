<?php
/**
 * DailyBox
 * Version 1.0.1
 * File items_model.php
 * Description CodeIgniter Model
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Items_model extends My_Model {
	
	function __construct()
	{
		parent::__construct();
	
		$this->table = 'items';
		$this->fields = array('proid', 'item', 'sortrank');
		$this->primary = 'itemid';
	}
	
	/**
	 * 查询数据
	 *
	 * @param array $options
	 * @return resource
	 */
	function get($options = array())
	{
		$fields = $this->_default($this->fields, array($this->primary));
		$this->db->select($fields);
		
		// 查询条件
		$condition = '';
		$qualificationArray = $fields;
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->where($qualifier, $options[$qualifier]);
			}
		}
		
		// 设置排序
		$this->db->order_by('sortrank', 'asc');
		$this->db->order_by($this->primary, 'asc');
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		if($query->num_rows() == 0) return false;
		
		// 返回记录集
		return $query->result();
	}
	
	/**
	 * 
	 * 获取某计划的所有项目
	 * 
	 * @param array $proids
	 * @return array
	 */
	function get_in_projects($proids = array())
	{
		$fields = $this->_default($this->fields, array($this->primary));
		$this->db->select($fields);
		
		if(!empty($proids))
		{
			$this->db->where_in('proid', $proids);
		}
		
		// 设置排序
		$this->db->order_by('sortrank', 'asc');
		$this->db->order_by($this->primary, 'asc');
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		if($query->num_rows() == 0) return false;
		
		// 返回记录集
		return $query->result();
	}
	
	/**
	 * 增加记录
	 *
	 * @param array $options
	 * @return int
	 */
	function add($options = array())
	{
		// 判断是否包含必须字段
		if($this->_required(array('proid', 'item'), $options) == false) return false;
		
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 执行查询
		$this->db->insert($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->insert_id();
	}
	
	/**
	 *  更新记录
	 *
	 * @param array $options
	 * @return int
	 */
	function update($options = array())
	{
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 设置更新条件
		$this->db->where($this->primary, $options[$this->primary]);
		
		// 执行查询
		$this->db->update($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
	
	/**
	 * 删除记录 
	 * 
	 * @param array $options
	 * @return int
	 */
	function delete($options = array())
	{
		// 设置字段
		$qualificationArray = $this->_default($this->fields, array($this->primary));
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->where($qualifier, $options[$qualifier]);
			}
		}
		
		// 提交查询
		$this->db->delete($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
}