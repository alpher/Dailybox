<?php
/**
 * DailyBox
 * Version 1.0.1
 * File projects_model.php
 * Description CodeIgniter Model
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects_model extends My_Model {
	
	function __construct()
	{
		parent::__construct();
	
		$this->table = 'projects';
		$this->fields = array('uid', 'project', 'score', 'memo', 'frequency', 'addtime', 'endtime');
		$this->primary = 'proid';
	}
	
	/**
	 * 查询数据
	 *
	 * @param array $options
	 * @return resource
	 */
	function get($options = array())
	{
		if(isset($options['fields']))
		{
			$fields = $options['fields'];
		}
		else 
		{
			$fields = $this->_default($this->fields, array($this->primary));
		}
		$this->db->select($fields);
		
		// OR查询条件
		$condition = '';
		$qualificationArray = array('project', 'memo');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				if(empty($condition))
				{
					$condition .=  " `{$qualifier}` like '%" . $this->db->escape_like_str($options[$qualifier]) . "%' ";
				}
				else 
				{
					$condition .=  " OR `{$qualifier}` like '%" . $this->db->escape_like_str($options[$qualifier]) . "%' ";
				}
			}
		}
		if(!empty($condition))
		{
			$this->db->where('(' . $condition . ')');
		}
		
		// AND查询语句
		$qualificationArray2 = array('proid', 'uid');
		foreach($qualificationArray2 as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->where($qualifier, $options[$qualifier]);
			}
		}
		
		// 设置排序
		$this->db->order_by($this->primary, 'desc');
		
		// 设置分页和返回记录数量
		if(isset($options['limit']) && isset($options['offset']))
		{
			$this->db->limit($options['limit'], $options['offset']);
		}
		else if(isset($options['limit']))
		{
			$this->db->limit($options['limit']);
		}
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		// 返回记录集
		return $query;
	}
	
	/**
	 * 
	 * 获得某日期内的计划
	 * 
	 * @param int $today
	 * @return array
	 */
	function get_availables($today)
	{
		$fields = $this->_default($this->fields, array($this->primary));
		$this->db->select($fields);
		
		$this->db->where('addtime <= ', $today);
		$this->db->where('endtime > ', $today);
		
		// 设置排序
		$this->db->order_by($this->primary, 'desc');
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		if($query->num_rows() == 0) return false;

		return $query->result();
	}
	
	/**
	 * 增加记录
	 *
	 * @param array $options
	 * @return int
	 */
	function add($options = array())
	{
		// 判断是否包含必须字段
		if($this->_required(array('project'), $options) == false) return false;
		
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 执行查询
		$this->db->insert($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->insert_id();
	}
	
	/**
	 *  更新记录
	 *
	 * @param array $options
	 * @return int
	 */
	function update($options = array())
	{
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 设置更新条件
		$this->db->where($this->primary, $options[$this->primary]);
		
		// 执行查询
		$this->db->update($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
	
	/**
	 * 删除记录 
	 * 
	 * @param array $options
	 * @return int
	 */
	function delete($options = array())
	{
		// 设置字段
		$qualificationArray = $this->_default($this->fields, array($this->primary));
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->where($qualifier, $options[$qualifier]);
			}
		}
		
		// 提交查询
		$this->db->delete($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
}