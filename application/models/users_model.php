<?php
/**
 * DailyBox
 * Version 1.0.1
 * File users_model.php
 * Description CodeIgniter Model
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/11
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends My_Model {

	function __construct()
	{
		parent::__construct();
		
		$this->table = 'users';
		$this->fields = array('username', 'pwd', 'salt', 'email');
		$this->primary = 'uid';
	}
	
	/**
	 * 获取用户信息
	 * 
	 * @param array $options
	 * @return boolean
	 */
	function get($options = array())
	{
		$fields = $this->_default($this->fields, array($this->primary));
		// 设置返回的字段列表
		$this->db->select($fields);
		
		// OR查询条件
		$condition = '';
		$qualificationArray = array('username', 'email');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				if(empty($condition))
				{
					$condition .=  " `{$qualifier}`=" . $this->db->escape($options[$qualifier]) . " ";
				}
				else 
				{
					$condition .=  " OR `{$qualifier}`=" . $this->db->escape($options[$qualifier]) . " ";
				}
			}
		}
		if(!empty($condition))
		{
			$this->db->where('(' . $condition . ')');
		}
		
		// AND查询语句
		if(isset($options[$this->primary]))
		{
			$this->db->where($this->primary, $options[$this->primary]);
		}
						
		// 设置排序
		$this->db->order_by($this->primary, 'asc');
		
		// 设置分页和返回记录数量
		if(isset($options['limit']) && isset($options['offset']))
		{
			$this->db->limit($options['limit'], $options['offset']);
		}
		else if(isset($options['limit']))
		{
			$this->db->limit($options['limit']);
		}
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		// 判断返回记录数
		if($query->num_rows() == 0) return false;
		
		// 返回记录集
		return $query->result();
		// 返回首条
		//return $query->row(0);
	}
	
	/**
	 * 记录是否已存在
	 * 
	 * @param array $options
	 * @return boolean
	 */
	function exists($options = array())
	{
		$this->db->select('count(1) as nums');
		
		// 设置查询条件
		$condition = '';
		$qualificationArray = array('username', 'email');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				if(empty($condition))
				{
					$condition .=  " `{$qualifier}`=" . $this->db->escape($options[$qualifier]) . " ";
				}
				else 
				{
					$condition .=  " OR `{$qualifier}`=" . $this->db->escape($options[$qualifier]) . " ";
				}
			}
		}
		if(!empty($condition))
		{
			$this->db->where('(' . $condition . ')');
		}
		
		$this->db->where('uid <>', $options[$this->primary]);
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		$rows = $query->row(0);
		
		return $rows->nums > 0;
	}
	
	/**
	 * 添加用户
	 * 
	 * @param array $options
	 * @return boolean
	 */
	function add($options = array())
	{
		// 判断是否包含必须字段
		if($this->_required(array('username', 'pwd', 'salt'), $options) == false) return false;
		
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 执行查询
		$this->db->insert($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->insert_id();
	}
	
	/**
	 * 更新用户数据
	 * 数组必须是名称键值对
	 * 
	 * @param array $data
	 * @return boolean
	 */
	function update($options = array())
	{
		// 判断是否包含必须字段
		if($this->_required($this->primary, $options) == false) return false;
		
		// 设置要更新的字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 设置查询条件
		$this->db->where($this->primary, $options[$this->primary]);
		
		// 执行查询
		$this->db->update($this->table);
		
		// 返回影响的记录数
		return $this->db->affected_rows();
	}
	
	/**
	 * 删除用户
	 * 
	 * @param array $options
	 */
	function delete($options = array())
	{
		
	}
}