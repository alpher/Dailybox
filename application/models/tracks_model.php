<?php
/**
 * DailyBox
 * Version 1.0.1
 * File tracks_model.php
 * Description CodeIgniter Model
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/27
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Tracks_model extends My_Model {
	
	function __construct()
	{
		parent::__construct();
	
		$this->table = 'tracks';
		$this->fields = array('itemid', 'note', 'state', 'notetime');
		$this->primary = 'id';
	}
	
	/**
	 * 查询数据
	 *
	 * @param array $options
	 * @return resource
	 */
	function get($options = array())
	{
		$fields = $this->_default($this->fields, array($this->primary));
		$this->db->select($fields);
		
		// 查询条件
		$condition = '';
		$qualificationArray = $fields;
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->where($qualifier, $options[$qualifier]);
			}
		}
		
		// 设置排序
		$this->db->order_by($this->primary, 'asc');
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		if($query->num_rows() == 0) return false;
		
		// 返回记录集
		return $query->result();
	}
	
	/**
	 * 获得某计划当日计划实施情况
	 *
	 * @param array options('itemids' => array('id1', 'id2', 'id3', ...'), 'start' => time, 'end' => time)
	 * @return array
	 */
	function get_tracks($options = array())
	{
		//$today_start = strtotime(date('Y-m-d', time()));
		//$today_end = $today_start + 86400;
		
		//$itemid = intval($itemid);
		//$in = '(' . implode(',', $itemids) . ')';
		
		$fields = $this->_default($this->fields, array($this->primary));
		$this->db->select($fields);
		
		// 条目ID
		$this->db->where_in('itemid', $options['itemids']);
		
		// 起始日期
		$this->db->where('notetime > ', $options['start']);
		
		// 结束日期
		$this->db->where('notetime <= ', $options['end']);
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		if($query->num_rows() == 0) return false;
		
		// 返回记录集
		return $query->result();
	}
	
	/**
	 * 处理某计划当日计划实施情况
	 *
	 * @param int $itemid
	 * @return boolean
	 */
	function set_tracks($itemid)
	{
		$today_start = strtotime(date('Y-m-d', time()));
		$today_end = $today_start + 86400;
		
		$itemid = intval($itemid);
		
		if($itemid <= 0) return false;
		
		// 判断是否记录过
		$fields = $this->_default($this->fields, array($this->primary));
		$this->db->select($fields);
		$this->db->where('itemid', $itemid);
		$this->db->where('notetime > ', $today_start);
		$this->db->where('notetime <= ', $today_end);
		
		// 提交查询
		$query = $this->db->get($this->table);
		
		if($query->num_rows() == 0)
		{
			$options = array(
					'itemid' => $itemid,
					'state' => 1,
					'notetime' => time(),
					'note' => ''
			);
			
			$id = $this->add($options);
			
			return ($id > 0) ? true : false;
		}
		else 
		{
			return true;
		}
	}
	
	/**
	 * 增加记录
	 *
	 * @param array $options
	 * @return int
	 */
	function add($options = array())
	{
		// 判断是否包含必须字段
		if($this->_required(array('itemid', 'state', 'notetime'), $options) == false) return false;
		
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 执行查询
		$this->db->insert($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->insert_id();
	}
	
	/**
	 *  更新记录
	 *
	 * @param array $options
	 * @return int
	 */
	function update($options = array())
	{
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 设置更新条件
		$this->db->where($this->primary, $options[$this->primary]);
		
		// 执行查询
		$this->db->update($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
	
	/**
	 * 删除记录 
	 * 
	 * @param array $options
	 * @return int
	 */
	function delete($options = array())
	{
		// 设置字段
		$qualificationArray = $this->_default($this->fields, array($this->primary));
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->where($qualifier, $options[$qualifier]);
			}
		}
		
		// 提交查询
		$this->db->delete($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
}