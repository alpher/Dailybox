<?php
/**
 * DailyBox
 * Version 1.0.1
 * File attachments_model.php
 * Description CodeIgniter Model
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/21
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Attachments_model extends My_Model {
	
	public function __construct() 
	{
		parent::__construct ();
		
		$this->table = 'attachments';
		$this->fields = array('title', 'contents', 'mimetype', 'extname', 'addtime', 'edittime');
		$this->primary = 'aid';
	}
	
	function get($options = array())
	{
		$fields = $this->_default($this->fields, array($this->primary));
		// 设置返回的字段列表
		$this->db->select($fields);

		// LIKE查询条件
		if(isset($options['title']))
		{
			$this->db->like('title', $options['title']);
		}
				
		// AND查询语句
		if(isset($options[$this->primary]))
		{
			$this->db->where($this->primary, $options[$this->primary]);
		}
		
		// 设置排序
		$this->db->order_by($this->primary, 'desc');
		
		// 设置分页和返回记录数量
		if(isset($options['limit']) && isset($options['offset']))
		{
			$this->db->limit($options['limit'], $options['offset']);
		}
		else if(isset($options['limit']))
		{
			$this->db->limit($options['limit']);
		}
		
		// 提交查询
		$query = $this->db->get('attachments');
		
		// 返回记录集
		return $query;
	}
	
	/**
	 * 添加附件
	 *
	 * @param array $options
	 * @return boolean
	 */
	function add($options = array())
	{
		// 判断是否包含必须字段
		if($this->_required(array('contents', 'mimetype', 'extname'), $options) == false) return false;
		
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		// 执行查询
		$this->db->insert($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->insert_id();
	}
	
	/**
	 *  更新记录
	 *
	 * @param array $options
	 * @return int
	 */
	function update($options = array())
	{
		// 设置字段
		$qualificationArray = $this->fields;
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->set($qualifier, $options[$qualifier]);
			}
		}
		
		$this->db->where($this->primary, $options[$this->primary]);
		
		// 执行查询
		$this->db->update($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
	
	/**
	 * 删除记录
	 *
	 * @param array $options
	 * @return int
	 */
	function delete($options = array())
	{
		// 设置字段
		$qualificationArray = $this->_default($this->fields, array($this->primary));
		
		// 给字段赋值
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				$this->db->where($qualifier, $options[$qualifier]);
			}
		}
		
		// 提交查询
		$this->db->delete($this->table);
		
		// 添加成功返回记录ID，不成功则返回 false
		return $this->db->affected_rows();
	}
}
