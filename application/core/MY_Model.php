<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/core/MY_Model.php
 * Description CodeIgniter My Model
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/12
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 扩展CodeIgniter自带的Model
 *
 * @author scriptfan
 *
 */
class My_Model extends CI_Model {
	
	var $table;
	var $fields = array();
	var $primary;
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	 * 如果指定的字段不存在，则返回错误
	 * 字段分配给$required参数
	 *
	 * @param array $required
	 * @param array $data
	 * @return bool
	 */
	function _required($required, $data)
	{
		foreach($required as $field) if(!isset($data[$field])) return false;
		return true;
	}
	
	/**
	 * 合并默认参数和可选参数
	 *
	 * @param array $defaults
	 * @param array $options
	 * @return array
	 */
	function _default($defaults, $options)
	{
		return array_merge($defaults, $options);
	}
	
	/**
	 * 设置数据表
	 * 
	 * @param string $table
	 */
	function _table($table)
	{
		$this->table = $table;
	}
	
	/**
	 * 设置字段
	 * 
	 * @param array $fields
	 */
	function _fields($fields = array())
	{
		$this->fields = $fields;
	}
}