<?php
/**
 * DailyBox
 * Version 1.0.1
 * File /application/core/MY_Controller.php
 * Description CodeIgniter My Controller
 * Author scriptfan
 * Email scriptfan@hotmail.com
 * Group qicaiyezi.com
 * Date 2016/02/07
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 扩展CodeIgniter自带的控制 器
 * 
 * @author scriptfan
 *
 */
class Base_Controller extends CI_Controller {
	var $theme;
	var $app_url;
	var $data = array();
	
	public function __construct()
	{
		parent::__construct();
	
		// 系统功能区
		$this->load->library('session');
		//$this->load->library('user_agent');
		$this->load->driver('cache');
		$this->load->helper('string');
		$this->load->helper('html');
		$this->load->helper('url');
		$this->load->helper('form');
		
		
		// 自定义区
		$this->load->model('events_model');
		
		$data['app_name']    = $this->config->item('app_name');
		$data['app_version'] = $this->config->item('app_version');
		$data['app_dir']     = $this->config->item('app_dir');
		$data['base_url']    = $this->config->item('base_url');
		$data['app_url']     = $this->app_url = $this->config->item('base_url') . 'index.php';
		
		// 平台判断
		/*
		if ($this->agent->is_mobile())
		{
		   $this->theme = 'mobile';
		}
		else 
		{
			$this->theme = 'default';
		}
		*/
		$this->theme = 'default';
		
		$data['theme'] = $this->theme;
		
		//合并数据
		$this->data = array_merge($this->data, $data);
		
		//全局输出
		$this->load->vars($data);
	}
	
	/**
	 *
	 * 获得计划跟踪的起始日期
	 *
	 * @param int $start (Timestamp)
	 * @param int $end (Timestamp)
	 * @param int $frequency
	 * @return int (Timestamp)
	 */
	public function getTrackTime($start, $end, $frequency)
	{
		if(empty($start) || empty($frequency)) return false;
	
		// 频率值折算为秒
		$frequency *= (24 * 3600);
	
		// 两个时间所包含的周期数
		$temp = ($end - $start) / $frequency;
	
		// 获得周期的余数，即当前所在周期
		while($temp > 1) $temp--;
	
		// 计算当前周期的起始日期
		$result = ($end - ($temp * $frequency));
	
		return $result;
	}
	
	/**
	 * 保存日志信息
	 * 
	 * @param string $title
	 * @param string $content
	 * @return NULL
	 */
	public function events($title, $content)
	{		
		if($this->config->item('open_event') === TRUE)
		{
			if(empty($title) || empty($content)) return;
			
			$ip = $this->input->ip_address();
			$addtime = time();
			
			$options = array(
				'title' => $title,
				'content' => $content,
				'ip' => $ip,
				'addtime' => $addtime
			);
			
			$this->events_model->add($options);
		}
	}
}

/**
 * 前台控制器
 * 
 * @author scriptfan
 *
 */
abstract class Front_Controller extends Base_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
}

/**
 * 后台控制器
 * 
 * @author scriptfan
 *
 */
abstract class Admin_Controller extends Base_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->check_login();
		
		$data['uid'] = $this->session->userdata('uid');
		$data['username'] = $this->session->userdata('username');

		//合并数据
		$this->data = array_merge($this->data, $data);
		
		$this->load->vars($data);
	}
	
	/**
	 * 判断是否登录
	 */
	public function check_login()
	{
		$username = $this->session->userdata('username');
		$islogin = $this->session->userdata('islogin');
		
		if(empty($username) || $islogin !== TRUE)
		{
			$message = urlencode('尚未登录或登录已超时');
		
			header('Location: '.$this->app_url.'/init/index/'.$message);
		}
	}
}