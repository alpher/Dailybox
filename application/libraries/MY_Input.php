<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class MY_Input extends CI_Input 
{  
  function _clean_input_keys($str)  
  {   
    $config = &get_config('config');   
    if (!preg_match("/^[".$config['permitted_uri_chars']."]+$/i", urlencode($str))) 
    {   
      exit('Disallowed Key Characters.');   
    }  
    
    return $str;  
  } 
}